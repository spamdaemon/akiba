#!/bin/sh

LIBS=`find libs -name \*.js`
SOURCES=`find src -name \*.js`

rm -f akiba.js;
touch akiba.js;

for f in $LIBS; do 
	cat $f >> akiba.js;
done;

for f in $SOURCES; do 
	cat $f >> akiba.js;
done;

#rm -rf ${HOME}/.config/google-chrome/Default/IndexedDB/http_localhost_0.indexeddb.leveldb
#rm -rf ${HOME}/.config/google-chrome/Default/IndexedDB/https_localhost_8080.indexeddb.leveldb


karma start --single-run --log-level debug
