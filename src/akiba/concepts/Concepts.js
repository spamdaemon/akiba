/**
 * 
 */
zone("akiba.concepts").service("Concepts", [ "akiba.generic.db.Concepts", "When", "Utils" ], function(Concepts, When, Utils) {
    "use strict";

    var provider = null;

    /**
     * Open the concepts database.
     * 
     * @return the concepts database
     */
    this.open = function() {
        if (provider === null) {
            console.log("Open concepts");
            provider = Concepts.open();
        }
        return provider;
    };

    this.close = function() {
        var tmp = provider;
        provider = null;
        return tmp.then(function(x) {
            return x.close();
        });
    };

    this.addConcepts = function(data) {
        return this.open().then(function(c) {
            c.addConcepts(data);
        });
    };
});
