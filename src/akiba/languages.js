/**
 * This class provides a registry for language factories.
 * <ul>
 * <li>addfactory(factory)</li>
 * <li>factoryFor(lang)</li>
 * <li>listLanguages()</li>
 * </ul>
 */
zone("akiba").service("Languages", [ "Utils" ], function(Utils) {

    var _languages = {};

    this.addLanguage = function(language) {
        var lang = language.language();
        _languages[lang] = language;
    };

    this.languageFor = function(lang) {
        return _languages[lang] || null;
    };

    this.listLanguages = function() {
        return Utils.keys(_languages);
    };
    this.clearLanguages = function() {
        _languages = {};
    };
});
