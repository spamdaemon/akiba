zone("akiba.nigiri").factory("promise", [ "When" ], function(When) {

    return function(request) {
        if (!request) {
            throw new Error("Not a request ")
        }

        return When.promise(function(resolve, reject, notify) {
            request.onsuccess = function() {
                resolve(request);
            };
            request.onerror = function() {
                reject(request);
            };
        });
    };

});

zone("akiba.nigiri").service("DBWrapper", [ "When", "promise", "nigiri.Nigiri" ], function(When, newPromise, Nigiri) {

    var provider = Nigiri.IndexedDB;

    /**
     * Extract the result of an operation.
     * 
     * @param {Request}
     *            request a request
     * @return {*} a result
     */
    var requestResult = function(request) {
        return request.result;
    };

    /**
     * Sort the data in place.
     * 
     * @param {Array}
     *            data an array
     * @return {Array} the data
     */
    this.sort = function(data) {
        // make a backup
        data = data.slice();
        data.sort(Nigiri.IndexedDB.cmp);
        return data;
    };

    this.openDatabase = function(name, version, onupgradeneeded) {
        var req = provider.open(name, version);
        req.onupgradeneeded = onupgradeneeded;
        return newPromise(req).then(requestResult);
    };

    /**
     * Clear the database and return a promise after it's been re-initialized.
     */
    this.deleteDatabase = function(name) {
        return newPromise(provider.deleteDatabase(name));
    };

    /**
     * Create a function that starts a promise.
     * 
     * @param stores
     *            a list of stores
     * @param mode
     *            the transaction mode
     * @return a function that takes a database and returns a transaction.
     */
    this.startTransaction = function(stores, mode) {
        return function(db) {
            var tx = db.transaction(stores, mode || "readonly");
            return tx;
        };
    };

    /**
     * Create a function that starts a promise.
     * 
     * @param stores
     *            a list of stores
     * @param mode
     *            the transaction mode
     * @return a function that takes a database and returns a transaction.
     */
    this.startTransactionRW = function(stores) {
        return this.startTransaction(stores, "readwrite");
    };

    /**
     * Create a function that starts a promise.
     * 
     * @param stores
     *            a list of stores
     * @param mode
     *            the transaction mode
     * @return a function that takes a database and returns a transaction.
     */
    this.startTransactionRO = function(stores) {
        return this.startTransaction(stores, "readonly");
    };

    /**
     * Execute a function within the context of a transaction.
     * 
     * @param stores
     *            a list of stores or a single store name
     * @param mode
     *            readonly or readwrite
     * @param f
     *            the function to execute (takes a transaction parameter)
     * @return return a function taking a database that when executed returns a promise
     */
    this.doTransaction = function(stores, mode, f) {
        if (!Array.isArray(stores)) {
            stores = [ stores ];
        }
        var self = this;
        return function(db) {
            return When.promise(function(resolve, reject, notify) {
                try {
                    var tx = db.transaction(stores, mode);
                    var promise = When(f(tx));
                    tx.oncomplete = function() {
                        resolve(promise);
                    };
                    tx.onerror = function() {
                        reject(new Error("Transaction failed"));
                    };
                } catch (error) {
                    console.log(error.stack);
                    console.log("Failed to do transaction " + JSON.stringify(stores));
                    reject(error);
                }
            });
        };
    };

    /**
     * Execute a function within the context of a transaction.
     * 
     * @param stores
     *            a list of stores
     * @param f
     *            the function to execute (takes a transaction parameter)
     * @return return a function taking a database that when executed returns a promise
     */
    this.doTransactionRW = function(stores, f) {
        return this.doTransaction(stores, "readwrite", f);
    };

    /**
     * Execute a function within the context of a transaction.
     * 
     * @param stores
     *            a list of stores
     * @param f
     *            the function to execute
     * @return a function that takes a database and returns a transaction.
     */
    this.doTransactionRO = function(stores, f) {
        return this.doTransaction(stores, "readonly", f);
    };

    /**
     * Store all objects in an object store.
     * 
     * @param tx
     *            a transaction object
     * @param store
     *            the name of a store
     * @param objects
     *            the object to be stored
     * @return a promise with containing objects
     */
    this.putAll = function(tx, store, objects) {
        if (objects.length === 0) {
            return When.resolve(objects);
        }
        var req = tx.objectStore(store).putAll(objects);
        return newPromise(req).yield(objects);
    };

    /**
     * Get all objects in an object store by id.
     * 
     * @param tx
     *            a transaction object
     * @param store
     *            the name of a store
     * @param ids
     *            the ids
     * @return a promise that will be resolved with the retrieved objects
     */
    this.getAll = function(tx, store, ids) {
        var req;
        if (arguments.length === 2) {
            req = tx.objectStore(store).getAll();
            ids = [];
        } else if (ids.length === 0) {
            return When.resolve([]);
        } else {
            ids = this.sort(ids);
            req = tx.objectStore(store).getAll(new Nigiri.KeySet(ids, true));
        }
        return newPromise(req).then(requestResult);
    };

    /**
     * Get all objects in an object store by id.
     * 
     * @param tx
     *            a transaction object
     * @param store
     *            the name of a store
     * @param ids
     *            the ids
     * @return a promise that will be resolved with id
     */
    this.update = function(tx, store, ids, fn) {
        var req;
        if (Array.isArray(ids)) {
            ids = this.sort(ids);
            req = tx.objectStore(store).update(fn, new Nigiri.KeyKeyset(ids));
        } else {
            req = tx.objectStore(store).update(fn, Nigiri.KeyRange.only(ids));
        }
        return newPromise(req).then(requestResult).yield(ids);
    };

    /**
     * Find one values in an index.
     * 
     * @param tx
     *            a transaction
     * @param store
     *            the store to query
     * @param index
     *            the name of an index
     * @param keys
     *            the keys used by the index
     * @return get the values objects
     */
    this.queryOneValue = function(tx, store, index, query, opts) {
        var options = {
            unique : true,
            limit : 1
        };
        opts = opts || {};

        if (opts.filter) {
            options.filter = function(cursor) {
                return opts.filter(cursor.value,cursor.key);
            };
        }
        if (opts.terminate) {
            options.terminate = function(cursor) {
                return opts.terminate(cursor.value,cursor.key);
            };
        }

        var req = tx.objectStore(store).index(index).getAll(query, new Nigiri.Options(options));
        return newPromise(req).then(requestResult);
    };

    /**
     * Find values in an index.
     * 
     * @param tx
     *            a transaction
     * @param store
     *            the store to query
     * @param index
     *            the name of an index
     * @param keys
     *            the keys used by the index
     * @return get the values objects
     */
    this.queryAllValues = function(tx, store, index, query, opts, optLimit) {
        var options = {
            unique : true
        };
        opts = opts || {};

        if (opts.filter) {
            options.filter = function(cursor) {
                return opts.filter(cursor.value,cursor.key);
            };
        }

        if (opts.terminate) {
            options.terminate = function(cursor) {
                return opts.terminate(cursor.value,cursor.key);
            };
        }
        if (opts.limit > 0) {
            options.limit = opts.limit;
        }
        if (options.filter || options.terminate) {
            options.withValues = true;
        }
        var req = tx.objectStore(store).index(index).getAll(query, new Nigiri.Options(options));
        return newPromise(req).then(requestResult);
    };

    /**
     * Find primary keys in an index.
     * 
     * @param tx
     *            a transaction
     * @param store
     *            the store to query
     * @param index
     *            the name of an index
     * @param keys
     *            the keys used by the index
     * @return get the values objects
     */
    this.queryAllKeys = function(tx, store, index, query, opts) {
        var options = {
            unique : true
        };
        opts = opts || {};

        if (opts.filter) {
            options.filter = function(cursor) {
                return opts.filter(cursor.value,cursor.key);
            };
        }

        if (opts.terminate) {
            options.terminate = function(cursor) {
                return opts.terminate(cursor.value,cursor.key);
            };
        }
        if (options.filter || options.terminate) {
            options.withValues = true;
        }
        if (opts.limit > 0) {
            options.limit = opts.limit;
        }
        var req = tx.objectStore(store).index(index).getAllPrimaryKeys(query, new Nigiri.Options(options));
        return newPromise(req).then(requestResult);
    };

    /**
     * Find values in an index.
     * 
     * @param tx
     *            a transaction
     * @param store
     *            the store to query
     * @param index
     *            the name of an index
     * @param keys
     *            the keys used by the index
     * @return get the values objects
     */
    this.findAllValues = function(tx, store, index, keys) {
        keys = this.sort(keys);
        var req = tx.objectStore(store).index(index).getAll(new Nigiri.KeySet(keys, true));
        return newPromise(req).then(requestResult);
    };

    /**
     * Get the primary keys in a index.
     * 
     * @param tx
     *            a transaction
     * @param store
     *            the store to query
     * @param index
     *            the name of an index
     * @param keys
     *            the keys used by the index
     * @return an index
     */
    this.findAllKeys = function(tx, store, index, keys) {
        keys = this.sort(keys);
        var req = tx.objectStore(store).index(index).getAllPrimaryKeys(new Nigiri.KeySet(keys, true));
        return newPromise(req).then(requestResult);
    };

    /**
     * Find values in an index.
     * 
     * @param tx
     *            a transaction
     * @param store
     *            the store to query
     * @param index
     *            the name of an index
     * @param keys
     *            the keys used by the index
     * @return get the values objects
     */
    this.getAllIndexKeys = function(tx, store, index) {
        var opts = {
            direction : "nextunique"
        };
        var req = tx.objectStore(store).index(index).getAllKeys(null, new Nigiri.Options(opts));
        return newPromise(req).then(requestResult).tap(function(x) {
            console.log("Found keys " + JSON.stringify(x));
        });
    };

});
