/**
 * 
 */
zone("akiba").service("Json", [], function() {
    "use strict";

    this.toString = function(obj) {
        return JSON.stringify(obj);
    };

    this.toPrettyString = function(obj) {
        return JSON.stringify(obj, null, 2);
    };

});