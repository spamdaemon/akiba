zone("akiba.quiz").create("standardQuiz").configure([ "akiba.generic" ]).service(
        "Factory",
        [ "Json", "When", "Utils", "translate", "createSkillSelector", "initializeWordSelector", "Variants", "updateScore" ],
        function(Json, When, Utils, translate, createSkillSelector, initializeWordSelector, Variants, updateScoreFN) {
            "use strict";

            var QUIZ_TYPE = "standardQuiz";

            /**
             * Choose a random value from a list of values
             * 
             * @param {*}
             *            an array or an object
             */
            var chooseRandomValue = function(values) {
                var result = Utils.sample(values, 1);
                return result.length > 0 && result[0] ? result[0] : null;
            };

            /**
             * Translate a word from a language to another language.
             * 
             * @param word
             *            a word id
             * @param from
             *            the language from which to translate
             * @param to
             *            the language into which to translate
             * @return a promise to a word in the "to" langauge;
             * 
             */
            var translateOneWord = function(word, from, to) {
                return translate(word, from, to).then(function(translations) {
                    return chooseRandomValue(translations);
                });
            };

            /**
             * Switch a list of variants withone where are default fields are null
             * 
             * @param variants
             * @return new variants
             */
            var createVariantForSelector = function(variant) {
                if (variant[0] === Variants.getDefault()) {
                    return [ null, variant[1] ];
                } else {
                    return variant;
                }
            };

            var selectorFromPresentation = function(spec, selector) {
                if (selector.value === null || selector.value === undefined) {
                    return null;
                }
                var result = initializeWordSelector(spec);
                if (selector.field === 'concept') {
                    result[selector.field] = selector.value;
                } else {
                    result[selector.field] = [ selector.value ];
                }
                console.log("Selector from presentation :" + JSON.stringify(result, null, 2));
                return result;
            };

            /**
             * Create a selector for a presentation. The selector can be used to locate all words that could be confused
             * with the presentation.
             * 
             * @param presentation
             *            a presentation
             * @return a selector
             */
            var createPresentationSelector = function(presentation, lang) {

                var result = {};
                switch (presentation.mode) {
                case 'read':
                    result.captureSkill = "read";
                    result.field = "read";
                    result.value = createVariantForSelector(presentation.text, lang);
                    break;
                case 'write':
                    result.captureSkill = "write";
                    result.field = "read";
                    // value will be filled in
                    break;
                case 'listen':
                    result.captureSkill = "listen";
                    result.field = "pronounce";
                    result.value = createVariantForSelector(presentation.pronounce, lang);
                    break;
                case 'speak':
                    result.captureSkill = "speak";
                    result.field = "read";
                    // value will be filled in
                    break;
                case 'show':
                    result.captureSkill = "recognize";
                    result.field = "concept";
                    result.value = [ presentation.concept ];
                    break;
                default:
                    throw new Error("Unsupported presentation mode " + presentation.mode);
                }
                return result;
            };

            var selectorFromCapture = function(capture) {
                var result = {};
                if (capture.field === 'concept') {
                    result[capture.field] = capture.value;
                } else {
                    result[capture.field] = [ [ capture.variant || null, capture.value ] ];
                }
                return result;
            };

            /**
             * Create a selector for a presentation. The selector can be used to locate all words that could be confused
             * with the presentation.
             * 
             * @param presentation
             *            a presentation
             * @return a selector
             */
            var createCapture = function(presentation, lang) {

                var result = {};

                switch (presentation.mode) {
                case 'read':
                    result.captureSkill = "read";
                    result.field = "read";
                    result.variant = presentation.text[0] === Variants.getDefault ? null : presentation.text[0];
                    result.value = presentation.text[1];
                    break;
                case 'write':
                    result.captureSkill = "write";
                    result.field = "read";
                    // value will be filled in
                    break;
                case 'listen':
                    result.captureSkill = "listen";
                    result.field = "pronounce";
                    result.variant = presentation.pronounce[0] === Variants.getDefault ? null : presentation.pronounce[0];
                    result.value = presentation.pronounce[1];
                    break;
                case 'speak':
                    result.captureSkill = "speak";
                    result.field = "read";
                    // value will be filled in
                    break;
                case 'show':
                    result.captureSkill = "recognize";
                    result.field = "concept";
                    result.value = [ presentation.concept ];
                    break;
                default:
                    throw new Error("Unsupported presentation mode " + presentation.mode);
                }
                return result;
            };

            /**
             * Create a selector for a presentation. The selector can be used to locate all words that could be confused
             * with the presentation.
             * 
             * @param presentation
             *            a presentation
             * @return a selector
             */
            var presentationMode2Skill = function(mode) {

                switch (mode) {
                case 'read':
                case 'write':
                case 'listen':
                case 'speak':
                    return mode;
                case 'show':
                    return 'recognize';
                default:
                    throw new Error("Unsupported presentation mode " + mode);
                }
            };

            var presentationVariant = function(presentation) {
                var result = null;
                switch (presentation.mode) {
                case 'read':
                case 'write':
                case 'listen':
                case 'speak':
                    result = presentation.selector.value[0];
                case 'show':
                    break;
                default:
                    throw new Error("Unsupported presentation mode " + presentation.mode);
                }
                return result;
            };

            /**
             * Determine if two presentations are the same.
             */
            var equalsPresentation = function(p1, p2) {
                if (p1.mode !== p2.mode) {
                    return false;
                }

                switch (p1.mode) {
                case 'show':
                    if (p1.concept !== p2.concept) {
                        return false;
                    }
                    break;
                case 'read':
                    if (p1.text[0] !== p2.text[0]) {
                        return false;
                    }
                    if (p1.text[1] !== p2.text[1]) {
                        return false;
                    }
                    break;
                case 'listen':
                    if (p1.pronounce[0] !== p2.pronounce[0]) {
                        return false;
                    }
                    if (p1.pronounce[1] !== p2.pronounce[1]) {
                        return false;
                    }
                    break;
                default:
                    break;
                }
                return true;
            };

            /**
             * Create the presentation for a list of words (by id)
             * 
             * @param words
             *            a list of word ids
             * @param lang
             *            the language in which the words are specified
             * @param spec
             *            the presentation spec
             * @return a promise
             */
            var createPresentation = function(words, lang, spec, forAnswer) {
                var result = null;
                var presentations = null;
                //console.log("Create Presentation "+spec.mode+" : " +JSON.stringify(words)+"\nvariants="+JSON.stringify(spec.variants));
                switch (spec.mode) {
                case 'show':
                    presentations = lang.imagesFor(words);
                    break;
                case 'read':
                    presentations = lang.textFor(words, spec.variants);
                    break;
                case 'listen':
                    presentations = lang.audioFor(words, spec.variants);
                    break;
                case 'write':
                    result = {
                        mode : spec.mode
                    };
                    if (spec.variants) {
                        result.variants = spec.variants;
                    }
                    break;
                case 'speak':
                    result = {
                        mode : spec.mode,
                    };
                    break;
                default:
                    throw new Error("Unknown mode " + spec.mode);
                }
                if (result !== null) {
                    if (forAnswer) {
                        result.capture = createCapture(result, lang);
                    } else {
                        result.selector = createPresentationSelector(result, lang);
                    }
                    return When.resolve(result);
                } else {

                    return presentations.then(function(prez) {
                        result = [];
                       Utils.each(words, function(w) {

                            var r = {
                                mode : spec.mode
                            };
                            switch (r.mode) {
                            case 'show':
                                r.images = prez[w].images;
                                r.concept = prez[w].concept;
                                if (r.images === null || r.images.length === 0) {
                                    r = null;
                                }
                                break;
                            case 'read':
                                r.text = chooseRandomValue(prez[w]);
                                if (r.text === null) {
                                    r = null;
                                }
                                break;
                            case 'listen':
                                r.audio = prez[w].audio;
                                r.pronounce = chooseRandomValue(prez[w].pronounce);
                                if (r.pronounce === null || r.pronounce.length === 0) {
                                    r = null;
                                }
                                break;
                            default:
                                throw new Error("Undefined mode " + spec.mode);
                            }
                            if (r) {
                                if (forAnswer) {

                                    r.capture = createCapture(r, lang);
                                    if (r.capture === null) {
                                        r = null;
                                    }
                                } else {
                                    r.selector = createPresentationSelector(r, lang);
                                    if (r.selector == null) {
                                        r = null;
                                    }
                                }
                            }
                            if (r) {
                                result.push(r);
                            }
                            else if (w===words[0]) {
                                throw new Error("Failed to create a presentation for the correct choice for answer "+forAnswer+"/"+spec.mode+": "+JSON.stringify(prez[w]));
                            }
                        });
                        return result;
                    });
                }
            };

            /**
             * Create the question structure.
             * 
             * @param word
             *            the word object
             * @param lang
             *            the language in which the word is defined
             * @param spec
             *            the question spec
             */
            var createQuestion = function(word, lang, spec) {
                return createPresentation([ word ], lang, spec).then(function(presentation) {
                    if (presentation.length === 0) {
                        reject(new Error("Failed to create presentation for question"));
                    }
                    var result = {
                        word : word,
                        mode : spec.mode,
                        language : lang.language(),
                        presentation : presentation[0]
                    };
                    return result;
                });
            };

            /**
             * Create the answer that must be entered by some means.
             * 
             * @param word
             *            the word object
             * @param lang
             *            the language in which the word is defined
             * @param spec
             *            the answer spec
             */
            var createFreeFormAnswer = function(word, lang, spec) {
                return When.promise(function(resolve, reject, notify) {
                    var pres = createPresentation([ word ], lang, spec, true).then(function(presentation) {
                        var result = {
                            word : word,
                            mode : spec.mode,
                            language : lang.language(),
                            presentation : presentation
                        };
                        return result;
                    });
                    resolve(pres);
                });
            };

            var getMaxConfusers = function(spec) {
                var N = (spec.choices > 1 ? spec.choices : 6) - 1;
                return N;
            };

            /**
             * Find words that could be confused with the question that was posed. Confusion may arise for example in
             * when the question is spoken and different words are pronounced the same way (e.g. son and sun)
             * 
             * @param nativeLanguage
             *            the native language
             * @param quiz
             *            the quiz
             * @return an array of confuser words
             */
            var findConfusers = function(foreignLanguage, nativeLanguage, skill, spec, qLanguage, qPresentation) {

                // first, get those words that could be confused with the question
                // e.g. "live" (as in live entertainment), or "live" as in "to live"
                var qConfusers = {};
                var confusers = selectorFromPresentation(spec, qPresentation);

                if (qLanguage === nativeLanguage.language()) {
                    qConfusers = nativeLanguage.findWords(confusers).then(function(w) {
                        return translate(w, nativeLanguage, foreignLanguage)["else"]([]);
                    });
                } else {
                    qConfusers = foreignLanguage.findWords(confusers);
                }
                return qConfusers;
            };

            var findAllPossibleAnswers = function(nativeLanguage, foreignLanguage, skill, spec, qLanguage, qPresentation) {
                var sTime = Date.now();

                // find the confusers in the foreignLanguage
                var candidates = findConfusers(foreignLanguage, nativeLanguage, skill, spec, qLanguage, qPresentation);

                // create the answer presentation for each word
                var answerLang = foreignLanguage;
                var answerSpec = spec.answer;

                if (answerSpec.mode === 'show' || answerSpec.language === nativeLanguage.language()) {
                    answerSpec.variants = null; // any variant will be fine for native languages
                    answerSpec.mode = 'read';
                    answerSpec.language = nativeLanguage.language();
                    answerLang = nativeLanguage;

                    candidates = candidates.then(function(c) {
                        return translate(c, foreignLanguage, nativeLanguage)["else"]([]);
                    });
                }

                if (answerSpec.mode === 'speak') {
                    answerSpec.mode = 'listen';
                } else {
                    answerSpec.mode = 'read';
                }

                return candidates.then(function(c) {
                    return createPresentation(c, answerLang, answerSpec, true);
                }).then(function(result) {

                    // remove duplicates from the list of candidates solutions;
                    var i, j, e;
                    for (i = 0; i < result.length; ++i) {
                        for (j = i + 1; j < result.length;) {
                            if (equalsPresentation(result[i], result[j])) {
                                e = result.pop();
                                if (j < result.length) {
                                    result[j] = e;
                                }
                            } else {
                                ++j;
                            }
                        }
                    }

                    result.timing = Date.now() - sTime;
                    return result;
                });
            };

            /**
             * Create a multiple choice answer structure.
             * 
             * @param word
             *            the word object
             * @param lang
             *            the language in which the word is defined
             * @param spec
             *            the answer spec
             * @param qconfusers
             *            the confusers based on the question
             * @param aconfusers
             *            the confusers based on the answer
             */
            var createMultipleChoiceAnswer = function(word, lang, spec, qconfusers, aconfusers) {
                var N = getMaxConfusers(spec);
                var confusers = Utils.union(qconfusers, aconfusers);
                confusers = Utils.shuffle(confusers);
                var allWords = [ word ].concat(confusers);

                // create presentations for all confusers; there may some presentations that we can't create
                return createPresentation(allWords, lang, spec, true).then(function(presentation) {
                    // mark the correct presentation before shuffling them
                    // FIXME: we should not set this here
                    presentation[0].correct = true;
                    if (confusers.length > N) {
                        presentation = presentation.slice(0, N + 1);
                    }
                    presentation = Utils.shuffle(presentation);
                    var result = {
                        word : word,
                        mode : "choose",
                        language : lang.language(),
                        presentation : presentation
                    };
                    return result;
                });

            };

            var createSelfTestAnswer = function(aSpec, possibleAnswers) {
                return When(possibleAnswers).then(function(a) {
                    return {
                        mode : "selfvalidate",
                        language : aSpec.language,
                        presentation : a
                    };
                });
            };

            /**
             * Create a question for a skill based on a spec.
             * 
             * @param skill
             *            the skill object for which to create a question
             * @param foreign
             *            the language associated with the skill
             * @param native
             *            the native language of the user
             * @return a promise that is resolved with a quiz, or rejected if a quiz could not be created
             */
            var createQuiz = function(skill, spec, foreign, native) {

                var question = null, answer = null;
                // by default we're working with skill's associated word
                var qword = When.resolve(skill.word);
                var qlang = foreign;
                var aword = qword;
                var alang = foreign;
                var qconfusers = When.resolve([]);
                var aconfusers = qconfusers;
                var media = null;

                // depending on the language of the question and the answer, we need to translate
                if (spec.question.language === 'native') {
                    qword = translateOneWord(skill.word, foreign, native);
                    qlang = native;
                }
                // once all promises have been resolved we can start assembling generating the question and answers
                question = qword.then(function(word) {
                    return createQuestion(word, qlang, spec.question);
                });

                // find the confusers for the question, which we will later use for the answer
                if (spec.answer.type === 'multiplechoice') {
                    if (spec.answer.language === 'native') {
                        aword = translateOneWord(skill.word, foreign, native);
                        alang = native;
                    }

                    if (spec.answer.mode === 'show') {
                        media = [ "image" ];
                    }
                    if (spec.question.mode === 'show') {
                        qconfusers = qword.then(function(w) {
                            return qlang.findConfusers(w, "recognize", null, media, getMaxConfusers(spec));
                        });
                    } else if (spec.question.mode === 'listen') {
                        qconfusers = qword.then(function(w) {
                            return qlang.findConfusers(w, spec.question.mode, null, media, getMaxConfusers(spec));
                        });
                    } else if (spec.question.mode === 'read') {
                        qconfusers = qword.then(function(w) {
                            return qlang.findConfusers(w, spec.question.mode, spec.question.variants, media, getMaxConfusers(spec));
                        });
                    }
                    if (spec.question.language !== spec.answer.language) {
                        qconfusers = qconfusers.then(function(words) {
                            if (words.length === 0) {
                                return [];
                            }
                            return translate(words, qlang, alang);
                        });
                    }

                    if (spec.question.mode === 'show') {
                        aconfusers = aword.then(function(w) {
                            return alang.findConfusers(w, "recognize", null, media, getMaxConfusers(spec));
                        });
                    } else if (spec.answer.mode === 'listen') {
                        aconfusers = aword.then(function(w) {
                            return alang.findConfusers(w, spec.answer.mode, null, media, getMaxConfusers(spec));
                        });
                    } else if (spec.answer.mode === 'read') {
                        aconfusers = aword.then(function(w) {
                            return alang.findConfusers(w, spec.answer.mode, spec.answer.variants, media, getMaxConfusers(spec));
                        });
                    }

                    answer = When.all([ aword, qconfusers, aconfusers ]).then(function(values) {
                        return createMultipleChoiceAnswer(values[0], alang, spec.answer, values[1], values[2]);
                    });
                } else if (spec.answer.type === 'selftest') {
                    answer = question.then(function(q) {
                        return findAllPossibleAnswers(native, foreign, skill, spec, spec.question.language, q.presentation.selector);
                    }).then(function(answers) {
                        return createSelfTestAnswer(spec.answer, answers);
                    });
                } else {

                    if (spec.answer.language === 'native') {
                        aword = translateOneWord(skill.word, foreign, native);
                        alang = native;
                    }

                    answer = aword.then(function(w) {
                        return createFreeFormAnswer(w, alang, spec.answer);
                    });
                }

                return When.all([ question, answer ]).then(function(values) {
                    return {
                        type : QUIZ_TYPE,
                        languages : {
                            foreign : foreign.language(),
                            native : native.language()
                        },
                        skill : skill.id,
                        question : values[0],
                        answer : values[1],
                        // the original spec
                        spec : spec
                    };
                });
            };

            // choose a foreign language skill to test based on the question and answer spec;
            // the result is a promise
            var chooseSkill = function(skills, spec) {
                var selector = createSkillSelector(spec);
                if (selector === null) {
                    return When.reject(new Error("Failed to create selector"));
                } else {
                    return skills.findSkill(selector.skill, function(s) {
                        return s + 60 * 1000;
                    }).then(function(skill) {
                        if (skill === null) {
                            return When.reject(new Error("Failed to create skill " + Json.toPrettyString(spec)));
                        } else {
                            return When.resolve(skill);
                        }
                    });
                }
            };

            /**
             * A constructor
             */
            var StandardQuiz = function(foreignLanguage, foreignSkills) {

                var createAnswerSelectors = function(confusers, captures) {
                    return When(confusers).then(function(confuserWords) {
                        var selectors = [];
                        var i, n, capture, selector;
                        if (confuserWords && confuserWords.length === 0) {
                            confuserWords = null;
                        }
                        for (i = 0, n = captures.length; i < n; ++i) {
                            capture = captures[i];
                            capture.correct = false;
                            selector = selectorFromCapture(capture);
                            if (selector && confuserWords) {
                                if (selector.id) {
                                    selector.id = Utils.intersection(selector.id, confuserWords);
                                } else {
                                    selector.id = confuserWords;
                                }
                            }

                            selectors.push(selector || {});
                        }
                        return selectors;
                    });
                };

                /**
                 * Check an answer.
                 * 
                 * @param questionConfusers
                 *            words that can possibly be confused with the answer.
                 * @param capture
                 *            the captured data
                 * @return a promise that updates the capture with the property correct
                 */
                var checkNativeAnswer = function(questionConfusers, nativeLanguage, capture) {

                    capture.correct = false;
                    var selector = selectorFromCapture(capture);
                    if (!selector) {
                        return When.resolve(capture);
                    }
                    // use confuser words in the selector!

                    return questionConfusers.then(function(confuserWords) {
                        var possibleAnswers = nativeLanguage.findWords(selector).then(function(w) {
                            return translate(w, nativeLanguage, foreignLanguage)["else"]([]);
                        }).then(function(answerWords) {
                            return Utils.intersection(answerWords, confuserWords);
                        });
                        return possibleAnswers.then(function(answerWords) {
                            capture.correct = answerWords.length > 0;
                            return capture;
                        });
                    });
                };

                /**
                 * Translate a word or a word list. It is allowed that both languages are the same, in which case this
                 * function just returns all words that have the same concept and will include the original word(s).
                 * 
                 * @param {Array.
                 *            <string>|string} a word list or a word id
                 * @param {*}
                 *            fromLang The language in which the words are specified
                 * @param {*}
                 *            toLang the language to which the words should be translated.
                 * @return {!Object} A promise object
                 */
                var xlate = function(wordList, confusers) {
                    var selectors = [];
                    Utils.each(wordList, function(words) {
                        var i, selector = {
                            concept : []
                        };
                        if (confusers) {
                            selector.id = confusers;
                        }
                        for (i = 0; i < words.length; ++i) {
                            selector.concept.push(words[i].concept);
                        }
                        selectors.push(selector);
                    });
                    return foreignLanguage.findWords(selectors);
                };

                /**
                 * Check an answer.
                 * 
                 * @param questionConfusers
                 *            words that can possibly be confused with the answer.
                 * @param capture
                 *            the captured data
                 * @return a promise that updates the capture with the property correct
                 */
                var checkNativeAnswers = function(questionConfusers, nativeLanguage, captures) {

                    // use confuser words in the selector!
                    return createAnswerSelectors(null, captures).then(function(selectors) {
                        return nativeLanguage.findWords(selectors, true);
                    }).then(function(wordList) {
                        return questionConfusers.then(function(confusers) {
                            return xlate(wordList, confusers);
                        });
                    }).then(function(answerWords) {
                        var i, n, capture;
                        for (i = 0; i < captures.length; ++i) {
                            captures[i].correct = answerWords[i].length > 0;
                        }
                        return captures;
                    });
                };

                /**
                 * Check an answer.
                 * 
                 * @param questionConfusers
                 *            words that can possibly be confused with the answer.
                 * @param capture
                 *            the captured data
                 * @return a promise that updates the capture with the property correct
                 */
                var checkForeignAnswers = function(questionConfusers, captures) {

                    // use confuser words in the selector!
                    return createAnswerSelectors(questionConfusers, captures).then(function(selectors) {
                        // console.log("FindForeignAnswer :" + JSON.stringify(selectors, null, 2));
                        return foreignLanguage.findWords(selectors).then(function(answerWords) {
                            var i, n, capture;
                            var correct = -1;
                            for (i = 0; i < captures.length; ++i) {
                                captures[i].correct = answerWords[i].length > 0;
                                if (captures[i].correct) {
                                    correct = i;
                                }
                            }
                            if (correct < 0) {
                                console.info("No correct answers found");
                            }
                            return captures;
                        });
                    });
                };

                this.language = function() {
                    return foreignLanguage;
                };

                this.type = function() {
                    return QUIZ_TYPE;
                };

                this.createQuestion = function(lang, spec) {
                    return When(lang).then(function(nativeLanguage) {
                        if (spec.type !== QUIZ_TYPE) {
                            throw new Error("Quiz type not supported " + spec.type);
                        }
                        if (nativeLanguage.language() === foreignLanguage.language()) {
                            throw new Error("Native and foreign languages must be different");
                        }
                        if (spec.question.mode === 'show' && spec.answer.mode == 'show') {
                            throw new Error("Question and answer must not be 'show'");
                        }

                        return chooseSkill(foreignSkills, spec).then(function(skill) {
                            return createQuiz(skill, spec, foreignLanguage, nativeLanguage);
                        });
                    });
                };

                /**
                 * Find all valid answers to the quiz question.
                 * 
                 * @param lang
                 *            the native language
                 * @param quiz
                 *            the quiz that was previously posed
                 * @return an array of answers
                 */
                this.findPossibleAnswers = function(lang, quiz) {
                    return When(lang).then(
                            function(nativeLanguage) {

                                return findAllPossibleAnswers(nativeLanguage, foreignLanguage, quiz.skill, Utils.cloneDeep(quiz.spec), quiz.question.language,
                                        quiz.question.presentation.selector);

                            });
                };

                this.answerQuestion = function(lang, quiz, captures) {
                    return When(lang).then(
                            function(nativeLanguage) {

                                var sTime = Date.now(), eTime, timing = {};

                                var qConfusers = findConfusers(foreignLanguage, nativeLanguage, quiz.skill, Utils.cloneDeep(quiz.spec), quiz.question.language,
                                        quiz.question.presentation.selector).tap(function() {
                                    eTime = Date.now();
                                    timing.confusers = eTime - sTime;
                                    sTime = eTime;
                                });

                                // check each captured object and set the correctness value
                                var captured = [];
                                // fixme: bottleneck
                                if (quiz.answer.language === nativeLanguage.language()) {
                                    captured = checkNativeAnswers(qConfusers, nativeLanguage, captures);
                                } else {
                                    captured = checkForeignAnswers(qConfusers, captures);
                                }
                                return When.all(captured).then(function(theCaptures) {
                                    var question = quiz.question;
                                    var skill = quiz.skill;

                                    eTime = Date.now();
                                    timing.checkAnswers = eTime - sTime;
                                    sTime = eTime;

                                    Utils.each(theCaptures, function(capture, i) {
                                        if (captures[i].submit) {

                                            var modeSkill, variant;
                                            var aPromise = When(true);
                                            if (question.language === nativeLanguage.language()) {
                                                modeSkill = capture.captureSkill;
                                                variant = capture.variant;
                                            } else {
                                                modeSkill = presentationMode2Skill(question.presentation.mode);
                                                variant = presentationVariant(question.presentation);

                                                if (modeSkill !== capture.captureSkill || variant != capture.variant) {
                                                    aPromise = foreignSkills.updateScore(skill, capture.captureSkill, capture.variant, function(score) {
                                                        updateScoreFN(score, capture.correct === true);
                                                    })['catch'](function(e) {
                                                        return true;
                                                    });
                                                }
                                            }
                                            // FIXME: possible that the score doesn't exist! e.g. no KATAKANA variant!
                                            // MAYBE reduce all variants for the skill!

                                            theCaptures[i] = aPromise.then(function() {
                                                return foreignSkills.updateScore(skill, modeSkill, variant, function(score) {
                                                    updateScoreFN(score, capture.correct === true);
                                                }).yield(capture)['catch'](function(error) {
                                                    return capture;
                                                });
                                            });
                                        }
                                    });

                                    return When.all(theCaptures).then(function(x) {
                                        eTime = Date.now();
                                        timing.updateAnswers = eTime - sTime;
                                        x[0].timing = timing;
                                        return x;
                                    });
                                });
                            });
                };
            };

            this.create = function(lang, skills) {
                return When(lang).then(function(l) {
                    return When(skills).then(function(s) {
                        if (l.language() !== s.language()) {
                            throw new Error("Language and skill mismatch " + l.language() + " vs. " + s.language());
                        }
                        return new StandardQuiz(l, s);
                    });
                });
            };
        });