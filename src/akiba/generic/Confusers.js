zone("akiba.generic").factory("Confusers", [ "Utils", "Variants" ], function(Utils, Variants) {

    var hasCommonPair = function(pairsA, pairsB) {
        var found = false;
        Utils.each(pairsA, function(pairA) {
            Utils.each(pairsB, function(pairB) {
                if (pairA[1] === pairB[1]) {
                    found = true;
                    return false;
                }
            });

            if (found) {
                return false;
            }
        });
        return found;
    };

    var isConfuser = function(word, confuser, mode, variants, media) {
        if (word.concept === confuser.concept || word.type !== confuser.type) {
            return false;
        }

        if (media) {
            if (!confuser.media || Utils.intersection(media, confuser.media).length === 0) {
                return false;
            }
        }
        
        if (mode === 'recognize') {
            return true;
        }
        
        var result = false;

        if (variants === null || variants.length === 0) {
            result = !hasCommonPair(word[mode], confuser[mode]);
        } else {
            Utils.each(variants, function(variant) {
                var wVariant = Variants.getVariant(word, mode, variant);
                if (wVariant === null) {
                    return;
                }
                var cVariant = Variants.getVariant(confuser, mode, variant);
                if (cVariant === null) {
                    return;
                }
                if (cVariant === wVariant) {
                    return;
                }
                // found a confuser, bail out early
                result = true;
                return false;
            });
        }
        return result;
    };

    return {
        isConfuser : isConfuser
    };
});
