/**
 * This function creates a basic language provider. Different languages use this to create the basic implementation for
 * the specific language.
 * <p>
 * A word selector has the following structure: *
 * 
 * <pre>
 *  id : the word's id
 *  type : type of word (e.g. letter, word, phrase, etc)
 *  concept : the concept implemented by this word
 *  tags : [ &lt;list of words&gt; ]
 *  pronounce : [
 *   [ &quot;&lt;variant&gt;&quot; , &quot;&lt;pronounciation&gt;&quot; ]
 *  ]
 *  read : [
 *    [ &quot;&lt;variant&gt;&quot; , &quot;&lt;text&gt;&quot; ]
 *  ]
 * </pre>
 * 
 */
zone("akiba.generic.db").service("Language",
        [ "Json", "When", "Utils", "nigiri.Nigiri", "DBWrapper", "akiba.concepts.Concepts", "Skills", "Variants", "Confusers", "WordSelector" ],
        function(Json, When, Utils, Nigiri, DB, Concepts, Skills, Variants, Confusers, WordSelector) {

            var RandomQuery = new Nigiri.EnumerableKeyRange(0, 1000000, function(x) {
                return x - 1;
            }, function(x) {
                return x + 1;
            });
            var random = function() {
                return Utils.random(RandomQuery.lower, RandomQuery.upper);
            };

            /**
             * The name of the concepts database.
             * 
             * @constant
             */
            var NAME = "words";

            /**
             * The version of the database
             * 
             * @constant
             */
            var VERSION = 1;

            /** The object store */
            var STORE = "words";

            var MEDIA_INDEX = "wordsWithMedia";
            var CONFUSER_INDEX = "confuserData";
            var TEXT_INDEX = "textIndex";
            var CONCEPT_INDEX = "conceptIndex";
            var VARIANT_INDEX = "variantsOfWords";

            // setup the database
            var openDB = function(lang) {
                return DB.openDatabase(NAME + lang, VERSION, function(event) {
                    var db = event.target.result;
                    var store = db.createObjectStore(STORE, {
                        keyPath : "id"
                    });
                    store.createIndex(MEDIA_INDEX, "media", {
                        unique : false,
                        multiEntry : true
                    });
                    store.createIndex(CONFUSER_INDEX, "__confusers", {
                        unique : false,
                        multiEntry : true
                    });
                    store.createIndex(TEXT_INDEX, "__text", {
                        unique : false,
                        multiEntry : true
                    });
                    store.createIndex(CONCEPT_INDEX, "concept", {
                        unique : false,
                        multiEntry : true
                    });
                    store.createIndex(VARIANT_INDEX, "__variants", {
                        unique : false,
                        multiEntry : true
                    });
                    console.log("Created words");
                });
            };

            /**
             * Clear the database and return a promise after it's been re-initialized.
             */
            var resetDB = function(lang) {
                return DB.deleteDatabase(NAME + lang).then(function() {
                    return openDB(lang);
                });
            };

            var checkForImages = function(conceptsDB, wordList) {
                var concepts = [];
                Utils.each(wordList, function(w) {
                    concepts.push(w.concept);
                });
                return conceptsDB.then(function(db) {
                    return db.checkImageFor(concepts);
                }).then(function(conceptsWithImages) {
                    var result = {};
                    Utils.each(wordList, function(w) {
                        if (conceptsWithImages[w.concept]) {
                            result[w.id] = true;
                        }
                    });
                    return result;
                });
            };

            var canGenerateAudio = function(lang, w, generator) {
                return generator && generator(lang, w);
            };

            var checkForAudio = function(lang, wordList, generator) {
                return When.promise(function(resolve, reject, notify) {

                    var result = {};
                    Utils.each(wordList, function(w) {
                        result[w.id] = canGenerateAudio(lang, w, generator);
                    });

                    resolve(result);
                });
            };

            var prepareForIndexing = function(w) {
                w.__variants = [];
                w.__confusers = [];

                w.__text = [];
                Utils.forEach(w.read, function(pair) {
                    w.__text.push(pair[1]);
                });
                Utils.forEach(w.pronounce, function(pair) {
                    w.__text.push(pair[1]);
                });
                w.__text = Utils.uniq(w.__text);

                Utils.each([ "read", "pronounce" ], function(s) {
                    Utils.each(w[s], function(entry) {
                        w.__variants.push(entry[0]);
                        w.__confusers.push([ w.type, entry[0], random() ]);
                    });
                });

                w.__variants = Utils.uniq(w.__variants);
            };

            /**
             * A constructor.
             * 
             * @constructor
             */
            var Language = function(concepts, language, audioGenerator) {
                var _concepts = concepts;
                var _allVariants = null;

                var _words = openDB(language);

                this.close = function() {
                    if (_words) {
                        var tmp = _words;
                        _words = null;
                        return tmp.then(function(db) {
                            db.close();
                            return true;
                        });
                    }
                    return When.resolve(true);
                };

                this.clear = function() {
                    _words = _words.then(function(db) {
                        db.close();
                        return resetDB(language);
                    });
                    return _words.then(function() {
                        return this;
                    });
                };

                this.language = function() {
                    return language;
                };

                /**
                 * Add a list of words to the language. If a word already exists. Each word has the following structure
                 * 
                 * <pre>
                 *  id : the word's id
                 *  type : type of word (e.g. letter, word, phrase, etc)
                 *  concept : the concept implemented by this word
                 *  pronounce : [
                 *   [ &quot;&lt;variant&gt;&quot; , &quot;&lt;pronounciation&gt;&quot; ]
                 *  ]
                 *  read : [
                 *    [ &quot;&lt;variant&gt;&quot; , &quot;&lt;text&gt;&quot; ]
                 *  ],
                 *  media : [ &quot;image&quot;, &quot;audio&quot; ]
                 * </pre>
                 * 
                 * @param words
                 *            an array of word objects (language specific)
                 * @return a promise that will be fullfilled with the words that were added (not necessarily in same
                 *         order as the input)
                 */
                this.addWords = function(words) {

                    if (words.length === 0) {
                        return When([]);
                    }
                    _allVariants = null;
                    words = Utils.cloneDeep(words);

                    var concepts = [];

                    // validate the words and normalize them
                    Utils.each(words, function(word) {
                        concepts.push({
                            id : word.concept
                        });

                        if (Utils.isUndefined(word.tags)) {
                            word.tags = [];
                        }

                        // check the variants
                        Utils.each(word.pronounce || [], function(pair) {
                            if (pair[0] === null) {
                                throw new Error("Invalid null variant");
                            }
                        });
                        Utils.each(word.read || [], function(pair) {
                            if (pair[0] === null) {
                                throw new Error("Invalid null variant");
                            }
                        });
                    });

                    // add the word's concept to the concepts database
                    var conceptsAdded = _concepts.then(function(db) {
                        return db.addConcepts(concepts);
                    });

                    // generate the media for the words
                    var audio = checkForAudio(this, words, audioGenerator);
                    var images = conceptsAdded.then(function() {
                        return checkForImages(_concepts, words);
                    });

                    return When.all([ audio, images ]).then(function(values) {
                        var mediaA = values[0];
                        var mediaI = values[1];
                        var result = [];

                        Utils.each(words, function(word) {
                            var media = [];
                            if (mediaA[word.id]) {
                                media.push("audio");
                            }
                            if (mediaI[word.id]) {
                                media.push("image");
                            }
                            word.media = media;

                            prepareForIndexing(word);

                            result.push(word);
                        });

                        return _words.then(DB.doTransactionRW(STORE, function(tx) {
                            return DB.putAll(tx, STORE, result);
                        }));
                    });
                };

                /**
                 * Load words into memory.
                 * 
                 * @param words
                 *            a list of word ids
                 * @return a promise that will be resolved with the word datastructures (indexed by word id)
                 */
                this.loadWords = function(words) {
                    return _words.then(DB.doTransactionRO(STORE, function(tx) {
                        return DB.getAll(tx, STORE, words);
                    })).then(function(list) {
                        var result = {};
                        Utils.each(list, function(item) {
                            result[item.id] = item;
                        });
                        return result;
                    });
                };

                /**
                 * Search for words that start with the specified text. This function is different from findWords in
                 * that it is optimized for the use case where a user types a partial word or phrase and we want to find
                 * the best matches.
                 * 
                 * @param text
                 *            a text string
                 * @return a list of word ids
                 */
                this.searchWords = function(text, maxResults) {
                    var q = Nigiri.KeyRange.lowerBound(text);

                    var startsWith = function(word) {
                        var result = false;
                        Utils.each(word.read, function(v) {
                            if (v[1].indexOf(text, 0) === 0) {
                                result = true;
                                return false; // done with iteration
                            }
                        });
                        return result;
                    };

                    return _words.then(DB.doTransactionRO(STORE, function(tx) {
                        return DB.queryAllKeys(tx, STORE, TEXT_INDEX, q, {
                            filter : startsWith,
                            terminate : function(w) {
                                return !startsWith(w);
                            },
                            limit : maxResults
                        });
                    }));
                };

                /**
                 * Get all words that have the specified concept. A selector may be of the form
                 * 
                 * <pre>
                 *   id : [ &lt;list of word ids&gt; ]
                 *   type : [ &lt;list of types&gt; ]
                 *   pronounce : [ [ &lt;list of variants&gt; , &quot;&lt;pronounciation&gt;&quot; ] ]
                 *   read : [ [ &lt;list of variants&gt; , , &quot;&lt;text&gt;&quot; ] ]
                 *   concept : [ &lt;list of concepts&gt; ]
                 *   tags : [ &lt;supported tags&gt; ]
                 * </pre>
                 * 
                 * @param selector
                 *            a selector
                 * @param byValue
                 *            true to return the values instead of just the word keys
                 * @return a promise that will be fullfilled with the ids of the words that have been found
                 */
                this.findWords = function(selectors, byValue) {

                    var selectorSet = Array.isArray(selectors) ? selectors : [ selectors ];
                    var selector = WordSelector.mergeSelectors(selectors);

                    // extract the text values
                    var text = [];
                    var fastQuery = selector !== null;
                    var textQuery = null;
                    var conceptQuery = null;
                    var queryOpts = {
                        unique : true,
                    };

                    if (fastQuery && selector.pronounce) {
                        Utils.forEach(selector.pronounce, function(p) {
                            if (p[1] === null) {
                                fastQuery = false;
                                return false;
                            }
                            text.push(p[1]);
                        });
                    }

                    if (fastQuery && selector.read) {
                        Utils.forEach(selector.read, function(p) {
                            if (p[1] === null) {
                                fastQuery = false;
                                return false;
                            }
                            text.push(p[1]);
                        });
                    }

                    if (fastQuery && selector.id) {
                        queryOpts.includedPrimaryKeys = selector.id.slice();
                    }

                    // for new, since we have an id, we don't need to
                    if (selector.id) {
                        // we'll actually just load those words in the set
                    } else if (fastQuery && text.length > 0) {
                        text = DB.sort(text);
                        console.log("Text : " + JSON.stringify(text));
                        textQuery = new Nigiri.KeySet(Utils.uniq(text, true));
                        queryOpts = new Nigiri.Options(queryOpts);
                    } else if (fastQuery && selector.concept && selector.concept.length > 0) {
                        selector.concept = DB.sort(selector.concept);
                        conceptQuery = new Nigiri.KeySet(Utils.uniq(selector.concept, true));
                        queryOpts = new Nigiri.Options(queryOpts);
                    }

                    var wordFilter = function(word) {
                        var i, n;
                        // console.log("Test WORD " + JSON.stringify(word, null, 2));
                        for (i = 0, n = selectorSet.length; i < n; ++i) {
                            if (WordSelector.matchWord(selectorSet[i], word)) {
                                // console.log("Matched selector " + JSON.stringify(selectorSet[i]));
                                return true;
                            }
                            // console.log("NOT Matched selector " + JSON.stringify(selectorSet[i]));
                        }
                        return false;
                    };

                    return _words.then(DB.doTransactionRO(STORE, function(tx) {
                        var values;

                        if (textQuery !== null) {
                            console.log("Executing fast textquery for " + JSON.stringify(selector));
                            if (selectors === selectorSet) {
                                values = DB.queryAllValues(tx, STORE, TEXT_INDEX, textQuery, {
                                    filter : wordFilter
                                });
                            } else if (byValue) {
                                return DB.queryAllValues(tx, STORE, TEXT_INDEX, textQuery, {
                                    filter : wordFilter
                                });
                            } else {
                                // getting here implies we only had a single selector object
                                return DB.queryAllKeys(tx, STORE, TEXT_INDEX, textQuery, {
                                    filter : wordFilter
                                });
                            }
                        } else if (conceptQuery !== null) {
                            console.log("Executing fast conceptquery for " + JSON.stringify(selector));
                            if (selectors === selectorSet) {
                                values = DB.queryAllValues(tx, STORE, CONCEPT_INDEX, conceptQuery, {
                                    filter : wordFilter
                                });
                            } else if (byValue) {
                                return DB.queryAllValues(tx, STORE, CONCEPT_INDEX, conceptQuery, {
                                    filter : wordFilter
                                });
                            } else {
                                // getting here implies we only had a single selector object
                                return DB.queryAllKeys(tx, STORE, CONCEPT_INDEX, conceptQuery, {
                                    filter : wordFilter
                                });
                            }
                        } else if (selector.id) {
                            console.log("Loading words by ID slow search for " + JSON.stringify(selector));
                            values = DB.getAll(tx, STORE, selector.id);
                        } else {
                            console.log("Executing slow search for " + JSON.stringify(selector));
                            values = DB.getAll(tx, STORE);
                        }

                        return values.then(function(words) {

                            var i, n, result = [];
                            for (i = 0, n = selectorSet.length; i < n; ++i) {
                                result.push([]);
                                Utils.each(words, function(word) {
                                    // we still need to check that each item in the set matches
                                    if (WordSelector.matchWord(selectorSet[i], word)) {
                                        if (byValue) {
                                            result[i].push(word);
                                        } else {
                                            result[i].push(word.id);
                                        }
                                    }
                                });
                            }
                            if (selectorSet === selectors) {
                                return result;
                            } else {
                                return result[0];
                            }
                        });
                    }));
                };

                /**
                 * Find words that a confusers of the specified word. This function will return a different results for
                 * identical queries due to randomization.
                 * 
                 * @param word
                 *            a word id
                 * @param mode
                 *            either READ or PRONOUNCE or RECOGNIZE
                 * @param variants
                 *            an optional array of variants of the specified mode
                 * @param media
                 *            any media that must be matched
                 * @param maxConfusers
                 *            the maximum number of confusers to return
                 * @return a promise that will be resolved with ids words that are similar, but have different meanings
                 */
                this.findConfusers = function(id, mode, variants, media, maxConfusers) {
                    return this.loadWords([ id ]).then(function(words) {
                        var theWord = words[id];
                        if (!theWord) {
                            return [];
                        }
                        variants = variants || null;

                        return _words.then(DB.doTransactionRW(STORE, function(tx) {
                            var numTested = 0;
                            var numFound = 0;

                            var allVariants = null;
                            if (variants !== null) {
                                allVariants = When(DB.sort(variants))
                            } else if (_allVariants) {
                                allVariants = When(_allVariants)
                            } else {
                                allVariants = DB.getAllIndexKeys(tx, STORE, VARIANT_INDEX).tap(function(v) {
                                    _allVariants = v;
                                });
                            }

                            return allVariants.then(function(vars) {
                                vars = new Nigiri.KeySet(vars);
                                var q = [ new Nigiri.KeySet([ theWord.type ]), vars, RandomQuery ];
                                return DB.queryAllValues(tx, STORE, CONFUSER_INDEX, new Nigiri.MultiKey(q), {
                                    filter : function(confuser) {
                                        numTested++;
                                        if (Confusers.isConfuser(theWord, confuser, mode, variants, media)) {
                                            // console.log("Found a confuser "+JSON.stringify(confuser));
                                            numFound++;
                                            return true;
                                        }

                                        return false;
                                    },
                                    limit : maxConfusers
                                })
                            }).tap(function() {
                                console.log("findConfusers: found " + numFound + " of " + numTested);
                            }).then(function(candidates) {
                                var result = [];
                                Utils.each(candidates, function(confuser) {
                                    // prepare the confuser for indexing so that we get a new random
                                    // value for it
                                    prepareForIndexing(confuser);
                                    result.push(confuser.id);
                                });
                                return DB.putAll(tx, STORE, candidates).yield(result);
                            });
                        }));
                    });
                };

                /**
                 * Get text for the specified words.
                 * 
                 * @param words
                 *            a list of word ids
                 * @param variants
                 *            the text variants
                 * @return a promise that will be resolved with a list of words and variants for each word
                 */
                this.textFor = function(words, variants) {
                    return _words.then(DB.doTransactionRO(STORE, function(tx) {
                        return DB.getAll(tx, STORE, words);
                    })).then(function(fixme) {
                        var result = {};
                        Utils.each(fixme, function(w) {
                            result[w.id] = Variants.variantsOf(w, "read", variants);
                            if (result[w.id].length === 0) {
                               // console.log("No text for "+JSON.stringify(w,null,3));
                            }
                        });
                        return result;
                    });
                };

                /**
                 * Get audio for the specified words.
                 * 
                 * @param words
                 *            a list of word ids
                 * @param variants
                 *            optional variants
                 * @return a promise that will be resolved with a list of audio urls for each word
                 */
                this.audioFor = function(words, variants) {
                    if (!audioGenerator) {
                        return When.reject(new Error("No audio generator configured"));
                    }

                    var lang = this;

                    return _words.then(DB.doTransactionRO(STORE, function(tx) {
                        return DB.getAll(tx, STORE, words);
                    })).then(function(fixme) {
                        var result = {};
                        Utils.each(fixme, function(w) {
                            result[w.id] = {
                                pronounce : Variants.variantsOf(w, "pronounce", variants),
                                audio : [ audioGenerator(lang, w) ]
                            };
                        });
                        return result;
                    });
                };

                /**
                 * Get images for each word.
                 * 
                 * @param words
                 *            a list of word ids
                 * @return a promise that will be resolved with a list of image urls for each word
                 */
                this.imagesFor = function(words) {
                    return _words.then(DB.doTransactionRO(STORE, function(tx) {
                        return DB.getAll(tx, STORE, words);
                    })).then(function(fixme) {

                        var concepts = [];
                        Utils.each(fixme, function(w) {
                            concepts.push(w.concept);
                        });
                        return _concepts.then(function(db) {
                            return db.imagesFor(concepts);
                        }).then(function(images) {
                            var result = {};
                            Utils.each(fixme, function(w) {
                                result[w.id] = {
                                    images : images[w.concept],
                                    concept : w.concept
                                };
                            });
                            return result;
                        });
                    });
                };
            };

            this.open = function(language, audioGenerator) {
                return When(new Language(Concepts.open(), language, audioGenerator));
            };

        });