/**
 * 
 */
zone("akiba.generic").create("db").configure([ "akiba.nigiri" ]);

zone("akiba.generic.db").service("Concepts", [ "When", "Utils", "DBWrapper" ], function(When, Utils, DB) {
    "use strict";

    /**
     * The name of the concepts database.
     * 
     * @constant
     */
    var NAME = "concepts";

    /**
     * The version of the database
     * 
     * @constant
     */
    var VERSION = 1;

    /** The object store */
    var STORE = "concepts";

    var INDEX = "conceptsWithImages";

    // setup the database
    var openDB = function() {
        return DB.openDatabase(NAME, VERSION, function(event) {
            var db = event.target.result;
            var store = db.createObjectStore(STORE, {
                keyPath : "id"
            });
            store.createIndex(INDEX, "__index.hasImages", {
                unique : false,
                multiEntry : false
            });
            console.log("Created concept store");
        });
    };

    /**
     * Clear the database and return a promise after it's been re-initialized.
     */
    var resetDB = function() {
        return DB.deleteDatabase(NAME).then(openDB);
    };

    var mergeImages = function(images1, images2) {
        var res = Utils.union(images1 || [], images2 || []);
        if (res.length === 0) {
            return null;
        } else {
            return res;
        }
    };

    var prepareForIndexing = function(concept) {
        delete concept.__index;
        if (concept.images && concept.images.length > 0) {
            concept.__index = {
                hasImages : concept.id
            };
        }
    };

    var Concepts = function() {

        /**
         * The concepts is a promise
         */
        var _concepts = openDB();

        /**
         * Get image URLs for a concept.
         * 
         * @param id
         *            a concept
         * @return an promise resolved concepts indexed by ids
         */
        this.loadConcepts = function(ids) {
            if (!Utils.isArray(ids)) {
                ids = [ ids ];
            }
            return _concepts.then(DB.doTransactionRO(STORE, function(tx) {
                return DB.getAll(tx, STORE, Utils.uniq(ids));
            }));
        };

        this.addConcepts = function(conceptList) {

            if (conceptList.length === 0) {
                return When([]);
            }

            conceptList = Utils.cloneDeep(conceptList);

            // collect the ids in the concepts
            var ids = [];
            var allconcepts = {};

            Utils.each(conceptList, function(concept) {
                if (!allconcepts[concept.id]) {
                    allconcepts[concept.id] = concept;
                    ids.push(concept.id);
                }
            });

            return _concepts.then(DB.doTransactionRW(STORE, function(tx) {

                // load all concepts that currently exist and merge them
                return DB.getAll(tx, STORE, ids).then(function(existingConcepts) {
                    Utils.each(existingConcepts, function(concept) {
                        concept.images = mergeImages(allconcepts[concept.id].images, concept.images);
                        allconcepts[concept.id] = concept;
                    });
                    return Utils.values(allconcepts);
                }).then(function(concepts) {
                    Utils.each(concepts, function(concept) {
                        concept.images = mergeImages(concept.images, []);
                        prepareForIndexing(concept);
                    });
                    return DB.putAll(tx, STORE, concepts);
                });
            }));
        };

        /**
         * Get image URLs for a concept.
         * 
         * @param id
         *            a concept
         * @return an promise resolved with images for each concept.
         */
        this.imagesFor = function(ids) {
            if (!Utils.isArray(ids)) {
                ids = [ ids ];
            }
            return _concepts.then(DB.doTransactionRO(STORE, function(tx) {
                return DB.findAllValues(tx, STORE, INDEX, ids);
            })).then(function(concepts) {
                var result = {};
                Utils.each(ids, function(id) {
                    result[id] = [];
                });
                Utils.each(concepts, function(concept) {
                    result[concept.id] = concept.images || [];
                });
                return result;
            })["catch"](function(error) {
                throw new Error("Failed imagesFor " + error);
            });
        };

        /**
         * Determine if a concept has at least 1 associated image.
         * 
         * @param id
         *            a concept
         * @return a promise resolved with an hash indicating which concept has an image
         */
        this.checkImageFor = function(ids) {

            if (!Utils.isArray(ids)) {
                ids = [ ids ];
            }
            return _concepts.then(DB.doTransactionRO(STORE, function(tx) {
                return DB.findAllKeys(tx, STORE, INDEX, Utils.uniq(ids));
            })).then(function(keys) {
                var result = {};
                Utils.each(ids, function(id) {
                    result[id] = false;
                });
                Utils.each(keys, function(id) {
                    result[id] = true;
                });
                return result;
            })["catch"](function(error) {
                throw new Error("Failed checkImageFor " + error.stack);
            });
        };

        this.close = function() {
            if (_concepts) {
                var tmp = _concepts;
                _concepts = null;
                return tmp.then(function(db) {
                    db.close();
                    return true;
                });
            }
            return When.resolve(true);
        };

        this.clear = function() {
            _concepts = _concepts.then(function(db) {
                db.close();
                return resetDB();
            });
            return _concepts.then(function() {
                return this;
            }, function() {
                return false;
            });
        };
    };

    this.open = function() {
        return When.resolve(new Concepts());
    };

});