/**
 * 
 * 
 */
zone("akiba.generic.db").service("Skills", [ "nigiri.Nigiri", "DBWrapper", "Json", "When", "Utils", "Variants", "SkillSelector" ],
        function(Nigiri, DB, Json, When, Utils, Variants, SkillSelector) {

            "use strict";

            /**
             * The name of the concepts database.
             * 
             * @constant
             */
            var NAME = "skills";

            /**
             * The version of the database
             * 
             * @constant
             */
            var VERSION = 1;

            /** The object store */
            var STORE = "skills";

            var MEDIA_INDEX = "skillsWithMedia";
            var TYPE_INDEX = "skillsByType";
            var VARIANT_INDEX = "variantsOfSkills";
            var SKILL_INDEX = "skillsByTime";

            // setup the database
            var openDB = function(lang) {
                var dbName = NAME + lang;
                return DB.openDatabase(dbName, VERSION, function(event) {
                    var db = event.target.result;
                    var store = db.createObjectStore(STORE, {
                        keyPath : "id"
                    });
                    store.createIndex(VARIANT_INDEX, "__variants", {
                        unique : false,
                        multiEntry : true
                    });
                    store.createIndex(SKILL_INDEX, "__indexedSkills", {
                        unique : false,
                        multiEntry : true
                    });
                    store.createIndex(MEDIA_INDEX, "media", {
                        unique : false,
                        multiEntry : true
                    });
                    store.createIndex(TYPE_INDEX, "type", {
                        unique : false,
                        multiEntry : false
                    });
                    console.log("Created skills " + dbName);
                });
            };

            var ALL_SKILLS = {
                read : true,
                write : true,
                listen : true,
                speak : true,
                recognize : true,
                translate : true
            };

            var nextInt = function(x) {
                return x + 1;
            };
            var predInt = function(x) {
                return x - 1;
            };

            var createSkillQuery = function(selectors, allTypes, allVariants) {
                console.log("Selectors " + JSON.stringify(selectors));
                var types = [];
                var variants = [];
                var skills = [];
                var defaultVariant = Variants.getDefault();
                Utils.each(selectors, function(selector) {
                    Utils.each(selector.skill, function(s) {
                        if (!Utils.isArray(s)) {
                            s = [ s, defaultVariant ];
                        }
                        if (s[0] !== null) {
                            skills.push(s[0]);
                        }
                        if (s[1] !== null) {
                            variants.push(s[1]);
                        } else if (allVariants) {
                            variants = variants.concat(allVariants[s[0]] || []);
                        }
                    });
                    if (selector.type) {
                        if (Utils.isArray(selector.type)) {
                            types = types.concat(selector.type);
                        } else {
                            types.push(selector.type);
                        }
                    } else if (allTypes) {
                        types = types.concat(allTypes);
                    }
                });
                types = DB.sort(Utils.uniq(types));
                variants = DB.sort(Utils.uniq(variants));
                skills = DB.sort(Utils.uniq(skills));
                var multi = [];

                if (variants.length === 0 && allVariants) {
                    variants = allVariants;
                }
                if (types.length === 0 && allTypes) {
                    types = allTypes;
                }

                console.log("types :" + JSON.stringify(types));
                console.log("variants :" + JSON.stringify(variants));
                console.log("skills :" + JSON.stringify(skills));
                if (types.length === 0 || variants.length === 0 || skills.length === 0) {
                    return null;
                }

                multi.push(new Nigiri.EnumerableKeyRange(0, 2 * Utils.now(), predInt, nextInt));
                multi.push(new Nigiri.KeySet(types));
                multi.push(new Nigiri.KeySet(skills));
                multi.push(new Nigiri.KeySet(variants));

                return new Nigiri.MultiKey(multi);
            };

            var prepareForIndexing = function(skill) {
                skill.__indexedSkills = [];
                skill.__variants = [];
                var defaultVariant = Variants.getDefault();

                Utils.each([ "listen", "read", "write" ], function(s) {
                    if (s === "listen" && !Utils.contains(skill.media, "audio")) {
                        return;
                    }
                    Utils.each(skill[s], function(score, variant) {
                        var index = [ score.nextTime, skill.type, s, variant ];
                        skill.__variants.push([ s, variant ]);
                        skill.__indexedSkills.push(index);
                    });
                });

                Utils.each([ "recognize", "speak", "translate" ], function(s) {
                    if (s === "recognize" && !Utils.contains(skill.media, "image")) {
                        return;
                    }
                    var score = skill[s];
                    if (score) {
                        skill.__variants.push([ s, defaultVariant ]);
                        skill.__indexedSkills.push([ score.nextTime, skill.type, s, defaultVariant ]);
                    }
                });

                skill.__variants = Utils.uniq(skill.__variants);
            };

            var createScore = function(time) {
                if (time < 0) {
                    time = Utils.now();
                }
                return {
                    nextTime : time,
                    lastTime : time,
                    score : 0,
                    count : 0
                };
            };

            var createSimpleSkill = function(object, skill, spec) {
                if (spec[skill] === true && Utils.isUndefined(object[skill])) {
                    object[skill] = createScore(-1);
                }
            };

            var createSkillWithVariants = function(data, object, skill, spec) {
                data = data || [];
                var variants = spec[skill];
                if (!Utils.isUndefined(variants) && variants !== false) {
                    if (variants === null || variants === true) {
                        variants = [];
                        Utils.each(data, function(pair) {
                            variants.push(pair[0]);
                        });
                    }
                    Utils.each(variants, function(variant) {
                        var s = object[skill] || {};
                        if (Utils.isUndefined(s[variant])) {
                            s[variant] = createScore(-1);
                        }
                        object[skill] = s;
                    });
                }
            };

            var createSkillID = function(wordID) {
                return wordID;
            };

            var createUpdateSkill = function(sid, oldSkill, word, skills) {
                var result = null;
                if (oldSkill !== null) {
                    result = Utils.cloneDeep(oldSkill);
                } else {
                    result = {
                        id : createSkillID(word.id),
                        word : word.id,
                    };
                }
                result.media = Utils.cloneDeep(word.media);
                result.tags = Utils.cloneDeep(word.tags);
                result.concept = word.concept;
                result.type = word.type;

                createSkillWithVariants(word.read, result, "read", skills);
                createSkillWithVariants(word.read, result, "write", skills);
                createSkillWithVariants(word.pronounce, result, "listen", skills);
                createSimpleSkill(result, "speak", skills);
                createSimpleSkill(result, "recognize", skills);
                createSimpleSkill(result, "translate", skills);

                result.media = word.media || [];

                prepareForIndexing(result);

                return result;
            };

            /**
             * Get all scores that can be found with the specified selector.
             * 
             * @param skill
             *            the skill whose scores to find
             * @param selector
             *            a selector for scores
             * @return a array of scores
             */
            var getScores = function(skill, selector) {
                var scores = [];
                Utils.each(selector, function(s) {
                    var tmp, score = null;
                    if (typeof s === 'string') {
                        tmp = skill[s];
                        if (!Utils.isUndefined(tmp)) {
                            scores.push(tmp);
                        }
                    } else if (s[0] === 'speak' || s[0] === 'recognize' || s[0] === 'translate') {
                        tmp = skill[s[0]];
                        if (!Utils.isUndefined(tmp)) {
                            scores.push(tmp);
                        }
                    } else {
                        tmp = skill[s[0]];
                        if (!Utils.isUndefined(tmp)) {
                            Utils.each(tmp, function(score, variant) {
                                if (s[1] === null || s[1] === variant) {
                                    scores.push(score);
                                }
                            });
                        }
                    }
                    if (score !== null && !score.hasOwnProperty("nextTime")) {
                        throw new Error("Did not retrieve a proper score " + JSON.stringify(skill));
                    }
                });
                return scores;
            };

            /**
             * Clear the database and return a promise after it's been re-initialized.
             */
            var resetDB = function(lang) {
                console.log("ResetDB " + NAME + lang);
                return DB.deleteDatabase(NAME + lang).then(function() {
                    return openDB(lang);
                });
            };

            var Skills = function(language) {

                var _skills = openDB(language);
                var _allVariants = null;
                var _allTypes = null;

                this.language = function() {
                    return language;
                };

                this.close = function() {
                    if (_skills) {
                        var tmp = _skills;
                        _skills = null;
                        return tmp.then(function(db) {
                            db.close();
                            return true;
                        });
                    }
                    return When.resolve(true);
                };

                this.clear = function() {
                    // reset the variants to null
                    _allVariants = null;
                    _allTypes = null;

                    _skills = _skills.then(function(db) {
                        db.close();
                        return resetDB(language);
                    });
                    return _skills.then(function() {
                        return this;
                    });
                };

                this.refreshSkills = function(words) {
                    // reset the variants to null
                    _allVariants = null;
                    _allTypes = null;

                    var wordsBySkill = {};
                    Utils.each(words, function(word) {
                        wordsBySkill[word.id] = word;
                    });

                    return _skills.then(DB.doTransactionRW(STORE, function(tx) {
                        var ids = Utils.keys(wordsBySkill);
                        return DB.getAll(tx, STORE, ids).then(function(oldSkills) {
                            var skillsToUpdate = {};

                            Utils.each(oldSkills, function(skill) {
                                var sid = skill.id;
                                var word = wordsBySkill[sid];

                                // get the skills
                                var skills = {
                                    read : !Utils.isUndefined(skill.read),
                                    write : !Utils.isUndefined(skill.write),
                                    listen : !Utils.isUndefined(skill.listen),
                                    speak : !Utils.isUndefined(skill.speak),
                                    recognize : !Utils.isUndefined(skill.recognize),
                                    translate : !Utils.isUndefined(skill.translate)
                                };
                                skill = createUpdateSkill(sid, skill, word, skills);
                                skillsToUpdate[sid] = skill;
                            });
                            return DB.putAll(tx, STORE, Utils.values(skillsToUpdate)).yield(Utils.keys(skillsToUpdate));
                        });
                    }));
                };

                /**
                 * Create or update the skills for a list of words. The skills is an object of the form
                 * 
                 * <pre>
                 *   read : [ &lt;variants&gt; ]
                 *   write : [ &lt;variants&gt; ]
                 *   listen : [ &lt;variants&gt; ]
                 *   speak : true|false
                 *   recognize : true|false,
                 *   translate : true|false
                 * </pre>
                 * 
                 * Additional skills properties may be supported depending on the language. If skills is null, then the
                 * skill for the word is deleted.
                 * 
                 * @param words
                 *            a list of word objects
                 * @param skills
                 *            the skills and their variants to be supported.
                 * @return a promise that will be fullfilled with a list of skill ids.
                 */
                this.updateSkills = function(words, skills) {

                    if (words.length === 0) {
                        return When([]);
                    }

                    // reset the variants to null
                    _allVariants = null;
                    _allTypes = null;

                    skills = skills || ALL_SKILLS;

                    var wordsBySkill = {};
                    Utils.each(words, function(word) {
                        wordsBySkill[word.id] = word;
                    });

                    return _skills.then(DB.doTransactionRW(STORE, function(tx) {
                        var ids = Utils.keys(wordsBySkill);

                        return DB.getAll(tx, STORE, ids).then(function(oldSkills) {
                            var skillsToUpdate = {};

                            Utils.each(oldSkills, function(oldSkill) {
                                var sid = oldSkill.id;
                                var s = createUpdateSkill(sid, oldSkill, wordsBySkill[sid], skills);
                                skillsToUpdate[sid] = s;
                            });

                            Utils.each(words, function(word) {
                                var sid = word.id;
                                if (!skillsToUpdate[sid]) {
                                    var s = createUpdateSkill(sid, null, word, skills);
                                    skillsToUpdate[sid] = s;
                                }
                            });

                            return DB.putAll(tx, STORE, Utils.values(skillsToUpdate)).then(function() {
                                return Utils.keys(skillsToUpdate);
                            });
                        });
                    }));
                };

                /**
                 * Load skills into memory.
                 * 
                 * @param skills
                 *            a list of word skills
                 * @return a promise that will be resolved with the word datastructures (indexed by word id)
                 */
                this.loadSkills = function(skills) {
                    return _skills.then(DB.doTransactionRO(STORE, function(tx) {
                        return DB.getAll(tx, STORE, skills);
                    })).then(function(list) {
                        var result = {};
                        Utils.each(list, function(item) {
                            result[item.id] = item;
                        });
                        return result;
                    });
                };

                /**
                 * Find a single skill. A selector is of the following form:
                 * 
                 * <pre>
                 *   word :  [ &lt;list of word ids&gt; ]
                 *   concept : [ &lt;list of word concepts&gt; ]
                 *   type : [ &lt;list of word types&gt; ]
                 *   skill : [ &lt;skill that must be supported&gt; , &lt; variant&gt; ]
                 * </pre>
                 * 
                 * 
                 * @param selectors
                 *            a list of skill selectors
                 * @param updateTimeFN
                 *            a function that can be used to update the time of the returned skill
                 * @return a promise that is fullfilled with the skill that matches all selectors
                 */
                this.findSkill = function(selectors, updateTimeFN) {

                    // find all selectors that match the specified skill and key
                    var checkSkill = function(skill, key) {
                        var result = [];
                        var skillKey = (key === null ? null : {});
                        // create a fake skill from a key so that each selector is forced
                        // to match or not match exactly the key;
                        if (skillKey) {
                            // the time is ignored
                            var x = {};
                            x[key[3]] = { /* the content doesn't matter */};
                            skillKey[key[2]] = x;
                            skillKey['type'] = key[1];
                        }

                        // we should actually return the skill and variant that the smallest score or
                        // nextTime
                        Utils.each(selectors, function(selector) {
                            if ((skillKey === null || SkillSelector.matchOneSkill(selector, skillKey)) && SkillSelector.matchSkill(selector, skill)) {
                                result.push(selector);
                            }
                        });
                        return result;
                    };

                    return _skills.then(DB.doTransactionRW(STORE, function(tx) {

                        var type_variants = [ null, null ];

                        var allSkillTypes = null;
                        var allSkillVariants = null;

                        if (_allTypes) {
                            type_variants[0] = When(_allTypes)
                        } else {
                            type_variants[0] = DB.getAllIndexKeys(tx, STORE, TYPE_INDEX).tap(function(v) {
                                _allTypes = v;
                            });
                        }

                        if (_allVariants) {
                            type_variants[1] = When(_allVariants)
                        } else {
                            type_variants[1] = DB.getAllIndexKeys(tx, STORE, VARIANT_INDEX).then(function(v) {
                                _allVariants = {};
                                Utils.each(v, function(e) {
                                    var s = e[0], variant = e[1];
                                    _allVariants[s] = (_allVariants[s] || []);
                                    _allVariants[s].push(variant);
                                });
                                return _allVariants;
                            });
                        }

                        return When.all(type_variants).then(function(tv) {

                            var q = createSkillQuery(selectors, tv[0], tv[1]);
                            if (q === null) {
                                console.log("Failed to create a skill query for " + JSON.stringify(selectors, null, 2));
                            }

                            var promise = null;
                            if (q) {
                                console.log("Executing query for skills");
                                promise = DB.queryOneValue(tx, STORE, SKILL_INDEX, q, {
                                    filter : function(v, key) {
                                        return checkSkill(v, key).length > 0;
                                    }
                                });
                            } else {
                                console.log("Getting all skills");
                                promise = DB.getAll(tx, STORE);
                            }
                            return promise;
                        }).then(function(allSkills) {
                            console.log("Found " + allSkills.length + " skills");

                            var result = null;

                            Utils.each(allSkills, function(skill) {
                                var matchingSelectors = checkSkill(skill, null);
                                var i, j, k, n = matchingSelectors.length, scores;
                                if (n === 0) {
                                    return;
                                }

                                // the skill becomes the result
                                result = skill;

                                // update score for each matching selector
                                if (updateTimeFN) {
                                    for (i = 0; i < n; ++i) {
                                        var scores = getScores(skill, matchingSelectors[i].skill);
                                        // FIXME: we may be updating some scores twice
                                        for (j = 0, k = scores.length; j < k; ++j) {
                                            scores[j].nextTime = updateTimeFN(scores[j].nextTime);
                                        }
                                    }
                                } else {
                                    console.log("No time update function specified");
                                }
                                return false;
                            });

                            // this actually updates the score!
                            if (updateTimeFN && result) {
                                prepareForIndexing(result);
                                // FIXME: write the skill back into the database
                                // console.log("Prepared for indexing\n" + JSON.stringify(result, null, 2));
                                return DB.putAll(tx, STORE, [ result ]).yield(result);
                            }

                            return result;
                        });
                    }));
                };

                /**
                 * Update the score associated with the specified skill.
                 * 
                 * @param id
                 *            the id of a skill
                 * @param skill
                 *            the type of skill (e.g. read or listen)
                 * @param variant
                 *            the optional variant
                 * @param f
                 *            a function that modifies the score (it takes a score and modifies the value)
                 * @return a promise that will be fullfilled with the id
                 */
                this.updateScore = function(id, skill, variant, updateFN) {

                    return _skills.then(DB.doTransactionRW(STORE, function(tx) {
                        var scoreSelector;
                        if (skill === 'speak' || skill === 'recognize' || skill === 'translate') {
                            scoreSelector = skill;
                        } else {
                            scoreSelector = [ skill, variant ];
                        }

                        var updateFunction = function(skillObject) {
                            var scores = getScores(skillObject, [ scoreSelector ]);
                            if (scores.length > 0) {
                                updateFN(scores[0]);
                                prepareForIndexing(skillObject);
                                return skillObject;
                            } else {
                                // don't update
                                return null;
                            }
                        };

                        return DB.update(tx, STORE, id, updateFunction);
                    }));
                };
            };
            this.open = function(language) {
                return When.resolve(new Skills(language));
            };

        });