/**
 * A generic translation function. This function translates words based on on concept equality. If two words have the
 * same concept then they are considered translations of each other.
 */
zone("akiba.generic").factory("translate", [ "Utils", "When" ], function(Utils, When) {

    /**
     * Translate a word or a word list. It is allowed that both languages are the same, in which case this function just
     * returns all words that have the same concept and will include the original word(s).
     * 
     * @param {Array.
     *            <string>|string} a word list or a word id
     * @param {*}
     *            fromLang The language in which the words are specified
     * @param {*}
     *            toLang the language to which the words should be translated.
     * @return {!Object} A promise object
     */
    var translate = function(wordOrWordList, fromLang, toLang) {

        if (Utils.isArray(wordOrWordList)) {
            return fromLang.loadWords(wordOrWordList).then(function(words) {
                var concepts = {};
                Utils.each(words, function(word) {
                    concepts[word.concept] = true;
                });

                var selector = {
                    concept : Utils.keys(concepts)
                };
                if (selector.concept.length === 0) {
                    return When.reject(new Error("No words"));
                }
                return toLang.findWords(selector);
            }).then(function(translations) {
                if (translations.length === 0) {
                    return When.reject(new Error("No translation found"));
                }
                return translations;
            });
        } else {
            return translate([ wordOrWordList ], fromLang, toLang)["catch"](function(error) {
                throw new Error("No translation found for " + wordOrWordList);
            });
        }
    };

    return translate;
});
