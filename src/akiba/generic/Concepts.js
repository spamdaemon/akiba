/**
 * 
 */
zone("akiba.generic").service("Concepts", [ "When", "Utils" ], function(When, Utils) {
    "use strict";

    var mergeImages = function(images1, images2) {
        var res = Utils.union(images1 || [], images2 || []);
        if (res.length === 0) {
            return null;
        } else {
            return res;
        }
    };

    var Concepts = function() {

        var _concepts = {};
        var _conceptsWithImages = {};

        this.addConcepts = function(concepts) {
            concepts = Utils.cloneDeep(concepts);

            return When.promise(function(resolve, reject, notify) {
                var result = [];
                Utils.each(concepts, function(concept) {

                    var current = _concepts[concept.id];

                    // if the concept exists, then keep it
                    if (current) {
                        current.images = mergeImages(current.images, concept.images);
                    } else {
                        _concepts[concept.id] = concept;
                        concept.images = mergeImages(concept.images, []);
                        current = concept;
                    }
                    result.push(current);

                    if (current.images) {
                        _conceptsWithImages[current.id] = true;
                    }
                });
                resolve(Utils.cloneDeep(result));
            });
        };

        /**
         * Get image URLs for a concept.
         * 
         * @param id
         *            a concept
         * @return an promise resolved concepts indexed by ids
         */
        this.loadConcepts = function(ids) {

            return When.promise(function(resolve, reject, notify) {

                var result = {};
                Utils.each(ids, function(id) {
                    if (_concepts[id]) {
                        result[id] = Utils.cloneDeep(_concepts[id]);
                    }
                });
                resolve(result);
            });
        };

        /**
         * Get image URLs for a concept.
         * 
         * @param id
         *            a concept
         * @return an promise resolved with images for each concept.
         */
        this.imagesFor = function(ids) {

            if (!Utils.isArray(ids)) {
                ids = [ ids ];
            }

            return When.promise(function(resolve, reject, notify) {

                var result = {};
                Utils.each(ids, function(id) {
                    if (_concepts[id]) {
                        result[id] = _concepts[id].images || [];
                    } else {
                        result[id] = [];
                    }
                });
                resolve(result);
            });
        };

        /**
         * Determine if a concept has at least 1 associated image.
         * 
         * @param id
         *            a concept
         * @return a promise resolved with an hash indicating which concept has an iamge
         */
        this.checkImageFor = function(ids) {

            if (!Utils.isArray(ids)) {
                ids = [ ids ];
            }
            return When.promise(function(resolve, reject, notify) {
                var result = {};
                Utils.each(ids, function(id) {
                    result[id] = _conceptsWithImages[id] === true;
                });
                resolve(result);
            });
        };

        this.clear = function() {
            return When.promise(function(resolve, reject, notify) {
                _concepts = {};
                _conceptsWithImages = {};
                resolve(this);
            });
        };

        this.close = function() {
            _concepts = null;
            return When.resolve(true);
        };
    };

    this.open = function() {
        return When.resolve(new Concepts());
    };
});