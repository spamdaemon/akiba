/**
 * 
 */
zone("akiba.generic").service("SkillSelector", [ "Utils" ], function(Utils) {

    var findOneSkill = function(skill, selectorSkill) {
        var skillFound = false;
        Utils.each(selectorSkill, function(s) {
            if (Utils.isArray(s)) {
                var tmp = skill[s[0]];
                if (Utils.isUndefined(tmp) || (s[1] !== null && Utils.isUndefined(tmp[s[1]]))) {
                    // skill not found
                    return true;
                }
            } else if (Utils.isUndefined(skill[s])) {
                // skill also not found
                return true;
            }
            skillFound = true;
            return false;
        });
        return skillFound;
    };

    var findAllSkills = function(skill, selectorSkill) {
        var allSkillsFound = true;
        Utils.each(selectorSkill, function(s) {
            if (Utils.isArray(s)) {
                var tmp = skill[s[0]];
                if (Utils.isUndefined(tmp) || (s[1] !== null && Utils.isUndefined(tmp[s[1]]))) {
                    allSkillsFound = false;
                    return false;
                }
            } else if (Utils.isUndefined(skill[s])) {
                allSkillsFound = false;
                return false;
            }
        });
        return allSkillsFound;
    };

    this.matchSkill = function(selector, skill) {
        if (selector.concept && skill.concept && !Utils.contains(selector.concept, skill.concept)) {
            // console.log("Wrong concept "+skill.concept);
            return false;
        }
        if (selector.type && skill.type && !Utils.contains(selector.type, skill.type)) {
            // console.log("Wrong type "+skill.type);
            return false;
        }
        if (selector.word && skill.word && !Utils.contains(selector.word, skill.word)) {
            // console.log("Wrong word "+skill.word);
            return false;
        }
        if (selector.tags && skill.tags && Utils.intersection(selector.tags, skill.tags).length === 0) {
            // console.log("Wrong tags "+JSON.stringify(skill.tags));
            return false;
        }
        // ALL media must be found
        if (selector.media && skill.media && Utils.intersection(selector.media, skill.media).length !== selector.media.length) {
            // console.log("Wrong media "+JSON.stringify(skill.media));
            return false;
        }
        if (selector.skill && skill && !findAllSkills(skill, selector.skill)) {
            // console.log("Wrong skill "+JSON.stringify( selector.skill));
            return false;
        }
        return true;
    };

    this.matchOneSkill = function(selector, skill) {
        return selector.skill && skill && findOneSkill(skill, selector.skill);
    };

    /**
     * Get all skills in this selector.
     */
    this.getSkills = function(selector) {
        var result = {};
        Utils.each(selector.skill, function(s) {
            if (Utils.isArray(s)) {
                var tmp = result[s[0]];
                if (s[1] === null) {
                    tmp = null;
                } else if (tmp === null) {
                    tmp = tmp || [];
                    tmp.push(s[1]);
                } else {
                    // ignore any
                }
                result[s[0]] = tmp;
            } else {
                result[s] = null;
            }
        });
        return result;
    }
});
