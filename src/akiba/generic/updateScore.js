/**
 * 
 */
zone("akiba.generic").factory("updateScore", [ "Utils", function(Utils) {

    return function(score, pass) {
        var now = Utils.now();
        if (pass) {
            score.score += 1;
            score.nextTime = now + 5*60 * 1000;
            score.lastTime = now;
        } else {
            // choose a random time
            score.nextTime += (30 + Math.random() * 120) * 1000;
        }
        score.count += 1;
    };
} ]);
