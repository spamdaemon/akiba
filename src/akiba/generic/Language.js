/**
 * This function creates a basic language provider. Different languages use this to create the basic implementation for
 * the specific language.
 * <p>
 * A word selector has the following structure: *
 * 
 * <pre>
 *  id : the word's id
 *  type : type of word (e.g. letter, word, phrase, etc)
 *  concept : the concept implemented by this word
 *  tags : [ &lt;list of words&gt; ]
 *  pronounce : [
 *   [ &quot;&lt;variant&gt;&quot; , &quot;&lt;pronounciation&gt;&quot; ]
 *  ]
 *  read : [
 *    [ &quot;&lt;variant&gt;&quot; , &quot;&lt;text&gt;&quot; ]
 *  ]
 * </pre>
 * 
 */
zone("akiba.generic").service("Language", [ "Json", "When", "Utils", "akiba.concepts.Concepts", "Skills", "Variants", "Confusers", "WordSelector" ],
        function(Json, When, Utils, Concepts, Skills, Variants, Confusers, WordSelector) {

            var checkForImages = function(conceptDB, wordList) {
                var concepts = [];
                Utils.each(wordList, function(w) {
                    concepts.push(w.concept);
                });
                return conceptDB.then(function(db) {
                    return db.checkImageFor(concepts);
                }).then(function(conceptsWithImages) {
                    var result = {};
                    Utils.each(wordList, function(w) {
                        if (conceptsWithImages[w.concept]) {
                            result[w.id] = true;
                        }
                    });
                    return result;
                });
            };

            var canGenerateAudio = function(lang, w, generator) {
                return generator && generator(lang, w);
            };

            var checkForAudio = function(lang, wordList, generator) {
                return When.promise(function(resolve, reject, notify) {

                    var result = {};
                    Utils.each(wordList, function(w) {
                        result[w.id] = canGenerateAudio(lang, w, generator);
                    });

                    resolve(result);
                });
            };

            /**
             * A constructor.
             * 
             * @constructor
             */
            var Language = function(concepts, language, audioGenerator) {

                var _words = {};
                var _concepts = concepts;

                this.clear = function() {
                    return When.promise(function(resolve, reject, notify) {
                        _words = {};
                        resolve(this);
                    });
                };

                this.close = function() {
                    _words = null;
                    return When.resolve(true);
                };

                this.language = function() {
                    return language;
                };

                /**
                 * Add a list of words to the language. If a word already exists. Each word has the following structure
                 * 
                 * <pre>
                 *  id : the word's id
                 *  type : type of word (e.g. letter, word, phrase, etc)
                 *  concept : the concept implemented by this word
                 *  pronounce : [
                 *   [ &quot;&lt;variant&gt;&quot; , &quot;&lt;pronounciation&gt;&quot; ]
                 *  ]
                 *  read : [
                 *    [ &quot;&lt;variant&gt;&quot; , &quot;&lt;text&gt;&quot; ]
                 *  ],
                 *  media : [ &quot;image&quot;, &quot;audio&quot; ]
                 * </pre>
                 * 
                 * @param words
                 *            an array of word objects (language specific)
                 * @return a promise that will be fullfilled with the words that were added (not necessarily in same
                 *         order as the input)
                 */
                this.addWords = function(words) {

                    words = Utils.cloneDeep(words);

                    var concepts = [];

                    // validate the words and normalize them
                    Utils.each(words, function(word) {
                        concepts.push({
                            id : word.concept
                        });

                        if (Utils.isUndefined(word.tags)) {
                            word.tags = [];
                        }

                        // check the variants
                        Utils.each(word.pronounce || [], function(pair) {
                            if (pair[0] === null) {
                                throw new Error("Invalid null variant");
                            }
                        });
                        Utils.each(word.read || [], function(pair) {
                            if (pair[0] === null) {
                                throw new Error("Invalid null variant");
                            }
                        });
                    });

                    // add the word's concept to the concepts database
                    var conceptsAdded = _concepts.then(function(db) {
                        return db.addConcepts(concepts);
                    });

                    var keys = {};
                    // generate the media for the words
                    var audio = checkForAudio(this, words, audioGenerator);
                    var images = conceptsAdded.then(function() {
                        return checkForImages(_concepts, words);
                    });

                    return When.all([ audio, images ]).then(function(values) {
                        var mediaA = values[0];
                        var mediaI = values[1];
                        var result = [];

                        Utils.each(words, function(word) {
                            var media = [];
                            if (mediaA[word.id]) {
                                media.push("audio");
                            }
                            if (mediaI[word.id]) {
                                media.push("image");
                            }
                            word.media = media;

                            _words[word.id] = word;
                            result.push(Utils.cloneDeep(word));
                        });

                        return result;
                    });
                };

                /**
                 * Search for words that start with the specified text.
                 * 
                 * @param text
                 *            a text string
                 * @return a list of word ids
                 */
                this.searchWords = function(text, maxResults) {
                    return When.promise(function(resolve, reject, notify) {
                        var result = [];
                        Utils.each(words, function(id) {
                            if (!Utils.isUndefined(_words[id])) {
                                Utils.each(_words[id].read, function(v) {
                                    if (v[1].indexOf(text, 0) === 0) {
                                        result.add(id);
                                        return false; // done with iteration
                                    }
                                });
                            }
                            if (result.length === maxResults) {
                                return false;
                            }
                        });
                        resolve(result);
                    });
                };

                /**
                 * Load words into memory.
                 * 
                 * @param words
                 *            a list of word ids
                 * @return a promise that will be resolved with the word datastructures (indexed by word id)
                 */
                this.loadWords = function(words) {
                    return When.promise(function(resolve, reject, notify) {
                        var result = {};
                        Utils.each(words, function(id) {
                            if (!Utils.isUndefined(_words[id])) {
                                result[id] = Utils.cloneDeep(_words[id]);
                            }
                        });
                        resolve(result);
                    });
                };

                /**
                 * Get all words that have the specified concept. A selector may be of the form
                 * 
                 * <pre>
                 *   id : [ &lt;list of word ids&gt; ]
                 *   type : [ &lt;list of types&gt; ]
                 *   pronounce : [ [ &lt;list of variants&gt; , &quot;&lt;pronounciation&gt;&quot; ] ]
                 *   read : [ [ &lt;list of variants&gt; , , &quot;&lt;text&gt;&quot; ] ]
                 *   concept : [ &lt;list of concepts&gt; ]
                 * </pre>
                 * 
                 * @param selector
                 *            a selector
                 * @param byValue
                 *            true to return values instead of keys
                 * @return a promise that will be fullfilled with the ids of the words that have been found
                 */
                this.findWords = function(selector, byValue) {
                    var selectorSet = Array.isArray(selector) ? selector : [ selector ];

                    return When.promise(function(resolve, reject, notify) {
                        var i, n, result = [];
                        for (i = 0, n = selectorSet.length; i < n; ++i) {
                            result.push([]);
                            Utils.each(_words, function(word) {
                                if (WordSelector.matchWord(selectorSet[i], word)) {
                                    if (byValue) {
                                        result[i].push(word);
                                    } else {
                                        result[i].push(word.id);
                                    }
                                }
                            });
                        }
                        if (selectorSet === selector) {
                            resolve(result);
                        } else {
                            resolve(result[0]);
                        }
                    });
                };

                /**
                 * Find words that a confusers of the specified word.
                 * 
                 * @param word
                 *            a word id
                 * @param mode
                 *            either READ or PRONOUNCE or RECOGNIZE
                 * @param variants
                 *            an optional array of variants of the specified mode
                 * @param media
                 *            any media that must be matched
                 * @return a promise that will be resolved with ids words that are similar, but have different meanings
                 */
                this.findConfusers = function(word, mode, variants, media, maxConfusers) {
                    return When.promise(function(resolve, reject, notify) {
                        var result = [];
                        variants = variants || null;

                        Utils.each(_words, function(confuser) {
                            if (Confusers.isConfuser(_words[word], confuser, mode, variants, media)) {
                                result.push(confuser.id);
                            }
                            if (result.length === maxConfusers) {
                                return false;
                            }
                        });
                        resolve(result);
                    });
                };

                /**
                 * Get text for the specified words.
                 * 
                 * @param words
                 *            a list of word ids
                 * @param variants
                 *            the text variants
                 * @return a promise that will be resolved with a list of words and variants for each word
                 */
                this.textFor = function(words, variants) {
                    return When.promise(function(resolve, reject, notify) {
                        var result = {};
                        Utils.each(words, function(w) {
                            if (_words[w]) {
                                result[w] = Variants.variantsOf(_words[w], "read", variants);
                            }
                        });
                        resolve(result);
                    });
                };

                /**
                 * Get audio for the specified words.
                 * 
                 * @param words
                 *            a list of word ids
                 * @param variants
                 *            optional variants
                 * @return a promise that will be resolved with a list of audio urls for each word
                 */
                this.audioFor = function(words, variants) {
                    if (!audioGenerator) {
                        return When.reject(new Error("No audio generator configured"));
                    }

                    var lang = this;

                    return When.promise(function(resolve, reject, notify) {
                        var result = {};
                        Utils.each(words, function(w) {
                            if (_words[w]) {
                                result[w] = {
                                    pronounce : Variants.variantsOf(_words[w], "pronounce", variants),
                                    audio : [ audioGenerator(lang, _words[w]) ]
                                };
                            }
                        });
                        resolve(result);
                    });
                };

                /**
                 * Get images for each word.
                 * 
                 * @param words
                 *            a list of word ids
                 * @return a promise that will be resolved with a list of image urls for each word
                 */
                this.imagesFor = function(words) {
                    return When.promise(function(resolve, reject, notify) {
                        var concepts = [];
                        Utils.each(words, function(w) {
                            if (_words[w]) {
                                concepts.push(_words[w].concept);
                            }
                        });
                        var r = _concepts.then(function(db) {
                            return db.imagesFor(concepts);
                        }).then(function(images) {
                            var result = {};
                            Utils.each(words, function(w) {
                                if (_words[w]) {
                                    result[w] = {
                                        images : images[_words[w].concept],
                                        concept : _words[w].concept
                                    };
                                }
                            });
                            return result;
                        });
                        resolve(r);
                    });
                };
            };

            this.open = function(language, audioGenerator) {
                return When(new Language(Concepts.open(), language, audioGenerator));
            };

        });