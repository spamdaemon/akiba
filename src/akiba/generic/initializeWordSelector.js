/**
 * A generic function that creates a selector for words from quiz specs.
 */
zone("akiba.generic").factory("initializeWordSelector", [ "Utils" ], function(Utils) {

    /**
     * Translate a word or a word list. It is allowed that both languages are the same, in which case this function just
     * returns all words that have the same concept and will include the original word(s).
     * 
     * @param {!Object}
     *            a quiz specification.
     * @return {Object|null} an object can be used to select skills in a language
     */
    return function(spec) {
        var selector = {};
        if (Array.isArray(spec.word.type)) {
            selector.type = spec.word.type;
        } else if (spec.word.type) {
            selector.type = [ spec.word.type ];
        }
        if (spec.word.tags) {
            selector.tags = spec.word.tags;
        }
        return selector;
    };
});
