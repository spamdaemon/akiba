/**
 * A generic function that creates a selector for skills.
 */
zone("akiba.generic").factory("createSkillSelector", [ "Utils" ], function(Utils) {

    var mode2Skill = function(mode) {
        switch (mode) {
        case "read":
        case "write":
        case "listen":
        case "speak":
            return mode;
        case "show":
            return "recognize";
        default:
            throw new Error("Unknown mode " + mode);
        }
    };

    /**
     * Translate a word or a word list. It is allowed that both languages are the same, in which case this function just
     * returns all words that have the same concept and will include the original word(s).
     * 
     * @param {!Object}
     *            a quiz specification.
     * @return {Object|null} an object can be used to select skills in a language
     */
    return function(spec) {
        // cannot choose a skill for a native language
        if (spec.question.language === 'native' && spec.answer.language === 'native') {
            return null;
        }

        var selector = {};
        var media = [];
        if (spec.word.type) {
            if (Utils.isArray(spec.word.type)) {
                selector.type = spec.word.type;
            } else {
                selector.type = [ spec.word.type ];
            }
        }
        if (spec.word.tags) {
            selector.tags = spec.word.tags;
        }

        if (spec.question.mode === 'show' || spec.answer.mode === 'show') {
            media.push("image");
        }
        if (spec.question.mode === 'listen' && spec.question.language !== 'native') {
            media.push("audio");
        }
        if (media.length > 0) {
            selector.media = media;
        }

        selector.skill = [];

        var qVariants = [];
        var aVariants = [];

        if (spec.question.language !== 'native') {
            if (spec.question.variants) {
                Utils.each(spec.question.variants, function(variant) {
                    qVariants.push([ mode2Skill(spec.question.mode), variant ]);
                });
            } else if (spec.question.mode === 'show') {
                qVariants.push(mode2Skill(spec.question.mode));
            } else {
                qVariants.push([ mode2Skill(spec.question.mode), null ]);
            }
        } else {
            qVariants.push([ "translate", null ]);
        }

        if (spec.answer.language !== 'native') {
            if (spec.answer.variants) {
                Utils.each(spec.answer.variants, function(variant) {
                    aVariants.push([ mode2Skill(spec.answer.mode), variant ]);
                });
            } else if (spec.answer.mode === 'speak' || spec.answer.mode === 'show') {
                aVariants.push(mode2Skill(spec.answer.mode));
            } else {
                aVariants.push([ mode2Skill(spec.answer.mode), null ]);
            }
        } else {
            aVariants.push([ "translate", null ]);
        }

        var selectors = [];

        for (var a = 0; a < aVariants.length; ++a) {
            for (var q = 0; q < qVariants.length; ++q) {
                var s = Utils.cloneDeep(selector);
                if (aVariants[a][0] !== "*") {
                    s.skill.push(aVariants[a]);
                }
                if (qVariants[q][0] !== "*") {
                    s.skill.push(qVariants[q]);
                }
                selectors.push(s);
            }

        }

        return {
            skill : selectors,
        };
    };
});
