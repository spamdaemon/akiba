/**
 * 
 */
zone("akiba.generic").service("WordSelector", [ "Variants", "Utils" ], function(Variants, Utils) {

    this.matchWord = function(selector, word) {
        if (selector.concept && !Utils.contains(selector.concept, word.concept)) {
            return false;
        }
        if (selector.type && !Utils.contains(selector.type, word.type)) {
            return false;
        }
        if (selector.tags && Utils.intersection(selector.tags, word.tags) === 0) {
            return false;
        }
        if (selector.media && Utils.intersection(selector.media, word.media) === 0) {
            return false;
        }
        if (selector.id && !Utils.contains(selector.id, word.id)) {
            return false;
        }
        if (selector.pronounce && !Variants.matchVariants(word.pronounce, selector.pronounce)) {
            return false;
        }
        if (selector.read && !Variants.matchVariants(word.read, selector.read)) {
            return false;
        }
        return true;
    };

    var mergeArrays = function(fromSelector, field, toSelector, mergable) {
        if (mergable[field] && fromSelector[field]) {
            toSelector[field] = fromSelector[field].concat(toSelector[field] || []);
        } else {
            mergable[field] = false;
            delete toSelector[field];
        }
    };

    var makeUnique = function(selector, field) {
        if (selector[field]) {
            selector[field].sort();
            selector[field] = Utils.uniq(selector[field]);
        }
    };

    /**
     * Merge multiple selectors into a selector that can be used to find a proper superset of each selector.
     * 
     * @return a selector or null if a super-selector cannot be created
     */
    this.mergeSelectors = function(selectors) {
        if (!Array.isArray(selectors)) {
            selectors = [ selectors ];
        }
        var i, n, s, selector = {};
        var mergable = {
            concept : true,
            type : true,
            tags : true,
            id : true,
            media : true,
            read : true,
            pronounce : true
        };

        for (i = 0, n = selectors.length; i < selectors.length; ++i) {
            s = selectors[i];
            mergeArrays(s, "concept", selector, mergable);
            mergeArrays(s, "type", selector, mergable);
            mergeArrays(s, "tags", selector, mergable);
            mergeArrays(s, "media", selector, mergable);
            mergeArrays(s, "id", selector, mergable);

            if (s.pronounce && mergable.pronounce) {
                selector.pronounce = Variants.mergeVariants(s.pronounce, selector.pronounce || []);
            } else {
                delete selector.pronounce;
                mergable.pronounce = false;
            }

            if (s.read && mergable.read) {
                selector.read = Variants.mergeVariants(s.read, selector.read || []);
            } else {
                delete selector.read;
                mergable.read = false;
            }
        }

        makeUnique(selector, "concept");
        makeUnique(selector, "type");
        makeUnique(selector, "media");
        makeUnique(selector, "tags");
        makeUnique(selector, "id");

        if (selector.concept || selector.type || selector.media || selector.tags || selector.id || selector.read || selector.pronounce) {
            return selector;
        } else {
            return null;
        }

    };

});
