/**
 * 
 * 
 */
zone("akiba.generic").service("Skills", [ "Json", "When", "Utils", "Variants", "SkillSelector" ], function(Json, When, Utils, Variants, SkillSelector) {

    var ALL_SKILLS = {
        read : true,
        write : true,
        listen : true,
        speak : true,
        recognize : true,
        translate : true
    };

    var createScore = function(time) {
        if (time < 0) {
            time = Utils.now();
        }
        return {
            nextTime : time,
            lastTime : time,
            score : 0,
            count : 0
        };
    };

    var createSimpleSkill = function(object, skill, spec) {
        if (spec[skill] === true && Utils.isUndefined(object[skill])) {
            object[skill] = createScore(-1);
        }
    };

    var createSkillWithVariants = function(data, object, skill, spec) {
        data = data || [];
        var variants = spec[skill];
        if (!Utils.isUndefined(variants) && variants !== false) {
            if (variants === null || variants === true) {
                variants = [];
                Utils.each(data, function(pair) {
                    variants.push(pair[0]);
                });
            }
            Utils.each(variants, function(variant) {
                var s = object[skill] || {};
                if (Utils.isUndefined(s[variant])) {
                    s[variant] = createScore(-1);
                }
                object[skill] = s;
            });
        }
    };

    var createSkillID = function(wordID) {
        return wordID;
    };

    var createUpdateSkill = function(sid, oldSkill, word, skills) {
        var result = null;
        if (oldSkill !== null) {
            result = Utils.cloneDeep(oldSkill);
        } else {
            result = {
                id : createSkillID(word.id),
                word : word.id,
            };
        }
        result.media = Utils.cloneDeep(word.media);
        result.tags = Utils.cloneDeep(word.tags);
        result.concept = word.concept;
        result.type = word.type;

        createSkillWithVariants(word.read, result, "read", skills);
        createSkillWithVariants(word.read, result, "write", skills);
        createSkillWithVariants(word.pronounce, result, "listen", skills);
        createSimpleSkill(result, "speak", skills);
        createSimpleSkill(result, "recognize", skills);
        createSimpleSkill(result, "translate", skills);

        result.media = word.media || [];

        return result;
    };

    /**
     * Get all scores that can be found with the specified selector.
     * 
     * @param skill
     *            the skill whose scores to find
     * @param selector
     *            a selector for scores
     * @return a array of scores
     */
    var getScores = function(skill, selector) {
        var scores = [];
        Utils.each(selector, function(s) {
            var tmp, score = null;
            if (s === 'speak' || s === 'recognize' || s === 'translate') {
                tmp = skill[s];
                if (!Utils.isUndefined(tmp)) {
                    scores.push(tmp);
                }
            } else {
                tmp = skill[s[0]];
                if (!Utils.isUndefined(tmp)) {
                    console.log("Get scores " + JSON.stringify(tmp));
                    Utils.each(tmp, function(score, variant) {
                        if (s[1] === null || s[1] === variant) {
                            scores.push(score);
                        }
                    });
                }
            }
            if (score !== null && !score.hasOwnProperty("nextTime")) {
                throw new Error("Did not retrieve a proper score " + JSON.stringify(skill));
            }
        });
        return scores;
    };

    var Skills = function(language) {

        var _skills = {};

        this.language = function() {
            return language;
        };

        this.clear = function() {
            _skills = {};
            return When.resolve(this);
        };

        this.close = function() {
            _skills = null;
            return When.resolve(true);
        };

        this.refreshSkills = function(words) {
            return When.promise(function(resolve, reject, notify) {
                var result = [];
                Utils.each(words, function(word) {
                    var sid = createSkillID(word.id);
                    var skill = _skills[sid];
                    if (Utils.isUndefined(skill)) {
                        return;
                    }

                    // get the skills
                    var skills = {
                        read : !Utils.isUndefined(skill.read),
                        write : !Utils.isUndefined(skill.write),
                        listen : !Utils.isUndefined(skill.listen),
                        speak : !Utils.isUndefined(skill.speak),
                        recognize : !Utils.isUndefined(skill.recognize),
                        translate : !Utils.isUndefined(skill.translate)
                    };
                    skill = createUpdateSkill(sid, skill, word, skills);
                    _skills[s.id] = skill;
                    result.push(sid);
                });
                resolve(result);
            });
        };

        /**
         * Create or update the skills for a list of words. The skills is an object of the form
         * 
         * <pre>
         *   read : [ &lt;variants&gt; ]
         *   write : [ &lt;variants&gt; ]
         *   listen : [ &lt;variants&gt; ]
         *   speak : true|false
         *   recognize : true|false,
         *   translate : true|false
         * </pre>
         * 
         * Additional skills properties may be supported depending on the language. If skills is null, then the skill
         * for the word is deleted.
         * 
         * @param words
         *            a list of word objects
         * @param skills
         *            the skills and their variants to be supported.
         * @return a promise that will be fullfilled with a list of skill ids.
         */
        this.updateSkills = function(words, skills) {
            skills = skills || ALL_SKILLS;

            return When.promise(function(resolve, reject, notify) {
                var result = {};
                Utils.each(words, function(word) {
                    var sid = word.id;
                    var s = createUpdateSkill(sid, _skills[sid] || null, word, skills);
                    _skills[s.id] = s;
                    result[s.id] = true;
                });
                resolve(Utils.keys(result));
            });
        };

        /**
         * Load skills into memory.
         * 
         * @param skills
         *            a list of word skills
         * @return a promise that will be resolved with the word datastructures (indexed by word id)
         */
        this.loadSkills = function(skills) {
            return When.promise(function(resolve, reject, notify) {
                var result = {};
                Utils.each(skills, function(id) {
                    if (!Utils.isUndefined(_skills[id])) {
                        result[id] = Utils.cloneDeep(_skills[id]);
                    }
                });
                resolve(result);
            });
        };

        /**
         * Find a single skill. A selector is of the following form:
         * 
         * <pre>
         *   word :  [ &lt;list of word ids&gt; ]
         *   concept : [ &lt;list of word concepts&gt; ]
         *   type : [ &lt;list of word types&gt; ]
         *   skill : [ &lt;skill that must be supported&gt; , &lt; variant&gt; ]
         * </pre>
         * 
         * A scoreSelector selector is required and <em>all</em> mentioned skills must match.
         * 
         * @param selectors
         *            a list of skill selectors
         * @param updateTimeFN
         *            a function that can be used to update the time of the returned skill
         * @return a promise that is fullfilled with the skill that matches all selectors
         */
        this.findSkill = function(selectors, updateTimeFN) {

            var checkSkill = function(skill) {
                var result = null;

                // we should actually return the skill and variant that the smallest score or
                // nextTime
                Utils.each(selectors, function(selector) {
                    if (SkillSelector.matchSkill(selector, skill)) {
                        result = selector;
                        return false;
                    }
                });
                return result;
            };

            return When.promise(function(resolve, reject, notify) {

                var result = null;
                var minScore = null;
                var minScores = null;
                var i, n;

                Utils.each(_skills, function(skill) {
                    var selector = checkSkill(skill);
                    if (selector === null) {
                        return;
                    }

                    var scores = getScores(skill, selector.skill);
                    var score = Utils.min(scores, function(s) {
                        return s.nextTime;
                    });

                    if (minScore === null || score.nextTime < minScore.nextTime) {
                        minScore = score;
                        minScores = scores;
                        result = skill;
                    }
                });
                if (result === null) {
                    resolve(null);
                    return;
                }

                // this actually updates the score!
                if (updateTimeFN) {
                    for (i = 0, n = minScores.length; i < n; ++i) {
                        minScores[i].nextTime = updateTimeFN(minScores[i].nextTime);
                    }
                }

                result = Utils.cloneDeep(result);
                resolve(result);
            });
        };

        /**
         * Update the score associated with the specified skill.
         * 
         * @param id
         *            the id of a skill
         * @param skill
         *            the type of skill (e.g. read or listen)
         * @param variant
         *            the optional variant
         * @param f
         *            a function that modifies the score (it takes a score and modifies the value)
         * @return a promise that will be fullfilled with the value true
         */
        this.updateScore = function(id, skill, variant, updateFN) {
            var scoreSelector;
            if (skill === 'speak' || skill === 'recognize' || skill === 'translate') {
                scoreSelector = skill;
            } else {
                scoreSelector = [ skill, variant ];
            }

            return When.promise(function(resolve, reject, notify) {
                var skillObject = _skills[id];
                if (!skillObject) {
                    reject(new Error("No such skill " + id));
                    return;
                }
                // get the score object
                var scores = getScores(skillObject, [ scoreSelector ]);
                if (scores.length > 0) {
                    updateFN(scores[0]);

                    // save the skill object after updating the score
                    resolve(true);
                } else {
                    reject(new Error("No such score " + Json.toPrettyString(scoreSelector)));
                }
            });
        };
    };

    this.open = function(language) {
        return When.resolve(new Skills(language));
    };

});