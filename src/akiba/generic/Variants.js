/**
 * 
 */
zone("akiba.generic").factory("Variants", [ "Json", "Utils" ], function(Json, Utils) {

    var DEFAULT_VARIANT = "";

    var variantsOf = function(word, mode, variants) {
        var result = [];
        var chooseAny = variants === null || Utils.isUndefined(variants) || Utils.contains(variants, null);
        if (word[mode]) {
            Utils.each(word[mode], function(pair) {
                if (chooseAny || pair[0] === DEFAULT_VARIANT || Utils.contains(variants, pair[0])) {
                    result.push([ pair[0], pair[1] ]);
                }
            });
        }
        return result;
    };

    var matchVariant = function(pair, template) {
        if (pair[0] !== DEFAULT_VARIANT && template[0] !== null && pair[0] !== template[0]) {
            return false;
        }
        if (template[1] !== null && pair[1] !== template[1]) {
            return false;
        }
        return true;
    };

    var matchVariants = function(variants, templates) {
        var found = false;
        Utils.each(templates, function(template) {
            Utils.each(variants, function(variant) {
                if (matchVariant(variant, template)) {
                    found = true;
                    return false;
                }
            });

            if (found) {
                return false;
            }
        });
        return found;
    };

    var getVariant = function(word, mode, variant) {
        var variants = word[mode];
        var result = null;
        var nullResult = null;
        if (variants) {
            Utils.forEach(variants, function(v) {
                // null is a default variant!
                if (v[0] === DEFAULT_VARIANT) {
                    nullResult = v[1];
                } else if (v[0] === variant) {
                    result = v[1];
                    return false;
                }
            });
        }
        if (result === null) {
            return nullResult;
        } else {
            return result;
        }
    };

    var compare = function(a, b) {
        if (a[0] < b[0]) {
            return -1;
        }
        if (a[0] > b[0]) {
            return 1;
        }
        if (a[1] < b[1]) {
            return -1;
        }
        if (a[1] > b[1]) {
            return 1;
        }
        return 0;
    };

    var equals = function(a, b) {
        return a[0] === b[0] && a[1] === b[1];
    };

    var mergeVariants = function(variantsA, variantsB) {
        var merged = (variantsA || []).concat(variantsB || []);
        if (merged.length === 0) {
            return merged;
        }
        merged.sort(compare);

        var i, j, n, res = [ merged[0] ];
        for (j = 0, i = 1, n = merged.length; i < n; ++i) {
            if (!equals(merged[j], merged[i])) {
                res.push(merged[i]);
                j = i;
            }
        }
        return res;
    };

    return {
        getDefault : function() {
            return DEFAULT_VARIANT;
        },
        getVariant : getVariant,
        matchVariants : matchVariants,
        variantsOf : variantsOf,
        mergeVariants : mergeVariants
    };
});
