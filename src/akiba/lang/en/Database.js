zone("akiba.lang.en").service("Database", [ "When", "akiba.generic.db.Language" ], function(When, Language) {
    "use strict";
    
    var provider = Language.open("en");

    this.open = function() {
        if (provider === null) {
            provider = Language.open("en");
        }
        return provider;
    };

    this.close = function() {
        var tmp = provider;
        provider = null;
        return tmp.then(function(x) {
            return x.close();
        });
    };

    this.addSkills = function(words) {
        return When.reject(new Error("No english skills available"));
    };

    this.addWords = function(data) {
        return this.open().then(function(lang) {
            return lang.addWords(data || []);
        });
    };
    this.searchWords = function(text, maxCount) {
        return this.open().then(function(lang) {
            return lang.searchWords(text, maxCount).then(lang.loadWords);
        });
    };

});
