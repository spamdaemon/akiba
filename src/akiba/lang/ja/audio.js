zone("akiba.lang.ja").factory("audio", [ "Utils" ], function(Utils) {

    // the LanguagePod101 site
    var site = "http://assets.languagepod101.com/dictionary/japanese/audiomp3.php?";

    var createLanguagePod101 = function(lang, word) {

        var uri = site;
        var kanji = lang.kanji(word);
        var kana = lang.kana(word);
        if (kanji) {
            uri += '&kanji=';
            uri += encodeURI(kanji);
        }
        if (kana) {
            uri += "&kana=";
            uri += encodeURI(kana);
        }
        return kana || kanji ? uri : null;
    };

    var createLetterSound = function(lang, word) {
        var romaji = lang.romaji(word);
        if (romaji) {
            return "data/audio/ja/" + romaji + ".mp3";
        } else {
            return null;
        }
    };

    return function(lang, word) {
        var result = word.audio;
        if (!result && word.type === 'letter') {
            result = createLetterSound(lang, word);
        }
        if (!result) {
            result = createLanguagePod101(lang, word);
        }
        return result;
    };

});
