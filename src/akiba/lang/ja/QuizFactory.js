zone("akiba.lang.ja").service("QuizFactory", [ "Database", "Skills" ], function(Database, Skills) {

    this.create = function(type) {
        type = type || "standardQuiz";
        var factory = "akiba.quiz." + type + ".Factory";

        return zone.inject([ factory ], function(QuizFactory) {
            var skills = Skills.open();
            var database = Database.open();

            return QuizFactory.create(database, skills);
        })();
    };
});
