zone("akiba.lang.ja").service("Skills", [ "akiba.generic.db.Skills" ], function(Skills) {

    var provider = null;

    this.open = function() {
        if (provider === null) {
            provider = Skills.open("ja");
        }
        return provider;
    };

    this.close = function() {
        var tmp = provider;
        provider = null;
        return tmp.then(function(x) {
            return x.close();
        });
    };

});
