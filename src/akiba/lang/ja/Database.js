zone("akiba.lang.ja").service("Database", [ "akiba.generic.db.Language", "Skills", "akiba.generic.Variants", "?audio" ],
        function(Language, Skills, Variants, audio) {
            "use strict";

            var extendLanguage = function(lang) {

                // let's add some more methods to the language
                lang.kanji = function(w) {
                    var variants = Variants.variantsOf(w, "read", [ "kanji" ]);
                    if (variants.length === 0) {
                        return null;
                    } else {
                        return variants[0][1];
                    }
                };

                // let's add some more methods to the language
                lang.kana = function(w) {
                    var variants = Variants.variantsOf(w, "read", [ "katakana", "hiragana" ]);
                    if (variants.length === 0) {
                        return null;
                    } else {
                        return variants[0][1];
                    }
                };

                // let's add some more methods to the language
                lang.hiragana = function(w) {
                    var variants = Variants.variantsOf(w, "read", [ "hiragana" ]);
                    if (variants.length === 0) {
                        return null;
                    } else {
                        return variants[0][1];
                    }
                };

                // let's add some more methods to the language
                lang.katakana = function(w) {
                    var variants = Variants.variantsOf(w, "read", [ "katakana" ]);
                    if (variants.length === 0) {
                        return null;
                    } else {
                        return variants[0][1];
                    }
                };

                lang.romaji = function(w) {
                    var variants = Variants.variantsOf(w, "read", [ "romaji" ]);
                    if (variants.length === 0) {
                        return null;
                    } else {
                        return variants[0][1];
                    }
                };

                return lang;
            };

            var createProvider = function() {
                return Language.open("ja", audio).then(extendLanguage);
            };

            var provider = null;

            this.open = function() {
                if (provider === null) {
                    provider = createProvider();
                }
                return provider;
            };

            this.close = function() {
                var tmp = provider;
                provider = null;
                return tmp.then(function(x) {
                    return x.close();
                }).then(function() {
                    return Skills.close();
                });
            };

            this.addSkills = function(words) {
                return Skills.open().then(function(s) {
                    return s.updateSkills(words, {
                        read : true,
                        write : true,
                        listen : true,
                        speak : true,
                        recognize : true,
                        translate : true
                    });
                });
            };

            this.addWords = function(data) {
                var theSkills = Skills.open();

                return this.open().then(function(lang) {
                    return lang.addWords(data || []);
                }).then(function(words) {
                    return theSkills.then(function(s) {
                        return s.refreshSkills(words);
                    }).then(function() {
                    }).yield(words);
                });
            };

            this.searchWords = function(text, maxCount) {
                return this.open().then(function(lang) {
                    return lang.searchWords(text, maxCount).then(lang.loadWords);
                });
            };

        });
