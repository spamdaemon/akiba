/**
 * This module setups externals for use with akiba. Externals are part of the akiba namespace
 */
zone().factory("#Utils", function() {
    return _.noConflict();
});
zone().factory("#When", function() {
    return when;
});
