this.db_setup = (function(global) {

    var Nigiri = zone.get("nigiri.Nigiri");
    var nextDB = 0;

    function is_array(o) {
        if (o != null && typeof o == "object") {
            return (typeof o.push == "undefined") ? false : true;
        } else {
            return false;
        }
    }

    var DBSetup = function(options) {
        options = options || {};
        this.__version = options.version || 1;
        this.__store = options.store || "store";
        this.__name = options.name || ("testDB" + (nextDB++));
        this.__options = options.options || {};
        this.__words = options.words || null;
        this.__concepts = options.concepts || null;
        this.__proficiencies = options.proficiencies || null;
        this.__prepopulate = options.prepopulate || false;
        this.db = null;
    };

    var prepopulate = function(self, done) {
        done();
    };

    var populate = function(self, done) {
        done();
    };

    DBSetup.prototype.setup = function() {
        var self = this;
        return function(done) {
            console.log(jasmine.getEnv().currentSpec.description);
            var db = Nigiri.IndexedDB;
            var req = db.deleteDatabase(self.__name);
            req.onsuccess = function() {
                if (self.__prepopulate) {
                    prepopulate(self, function() {
                        populate(self, done);
                    });
                } else {
                    populate(self, done);
                }

            };
            req.onerror = function() {
                // console.log("Setup " + self.__name + " : DeleteDB FAIL");
                expect(true).toBe(false);
                done();
            };
        };
    };

    DBSetup.prototype.teardown = function() {
        var self = this;
        return function(done) {
            if (self.db) {
                self.db.close();
                self.db = null;
            }
            done();
            return;
            var req = Nigiri.IndexedDB.deleteDatabase(self.__name);
            req.onsuccess = function() {
                // console.log("Teardown " + self.__name + " : OK");
                done();
            };
            req.onerror = function() {
                // console.log("Teardown " + self.__name + " : FAIL");
                done();
            };
        };
    };

    return DBSetup;
})(this);