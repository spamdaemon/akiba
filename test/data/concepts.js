zone("test.akiba.concepts").factory("data", function() {

    var data = [ {
        "id" : "魚",
        "images" : [ "data/images/fish/fish.png", "data/images/fish/1_fish.png" ]
    }, {
        "id" : "tree",
        "images" : [ "data/images/tree/tree.jpg" ]
    }, {
        "id" : "timber",
        "images" : [ "data/images/tree/tree.jpg" ]
    }, {
        "id" : "wood",
        "images" : [ "data/images/tree/tree.jpg" ]
    }, {
        "id" : "car",
        "images" : [ "data/images/car/car.png", "data/images/car/hp_auto.png" ]
    }, {
        "id" : "cat",
        "images" : [ "data/images/cat/cat.png" ]
    }, {
        "id" : "dog",
        "images" : [ "data/images/dog/dog.png" ]
    }, {
        "id" : "horse",
        "images" : [ "data/images/horse/horse.jpg" ]
    }, {
        "id" : "house",
        "images" : [ "data/images/house/house1.png", "data/images/house/house2.png" ]
    } ];
    
    return data;
});
