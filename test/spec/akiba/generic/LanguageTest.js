describe("Language", function() {
    // set up the async spec
    var async = new AsyncSpec(this);

    var printResult = function(result) {
        console.log(JSON.stringify(result, null, 2));
        return result;
    };

    var getWordIds = function(words) {
        var result = [];
        var i, n;
        for (i = 0, n = words.length; i < n; ++i) {
            result.push(words[i].id);
        }
        return result;
    };

    var English = null;
    var englishWords = null;
    var Variants = null;

    async.beforeEach(function(done) {
        // also creates the akiba.ut package
        Variants = zone("akiba.generic").get("Variants");

        englishWords = [ {
            id : "1",
            type : "word",
            concept : "CENTER",
            tags : [ "noun", "adjective" ],
            pronounce : [ [ Variants.getDefault(), "center" ] ],
            read : [ [ "us", "center" ], [ "uk", "centre" ] ]
        }, {
            id : "2",
            type : "phrase",
            concept : "Good Morning",
            pronounce : [ [ Variants.getDefault(), "Good Morning" ] ],
            read : [ [ Variants.getDefault(), "Good Morning" ] ]
        }, {
            id : "confuser",
            type : "word",
            tags : [ "verb", "adjective" ],
            concept : "confuser",
            pronounce : [ [ Variants.getDefault(), "confuser" ] ],
            read : [ [ Variants.getDefault(), "confuser" ] ]
        } ];

        zone("akiba.generic").get("Language").open("en").then(function(lang) {
            English = lang;
            return lang.clear().then(done);
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
            done();
        });
    });

    async.afterEach(function(done) {
        English.close().then(done);
    });
    
    async.it("create a language provider", zone.inject("akiba.generic", [ "#done", "Language" ], function(done, Language) {
        Language.open("ja").done(function(lang) {
            expect(lang.language()).toBe("ja");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
            done();
        });
    }));

    async.it("add words", function(done) {
        English.addWords(englishWords).then(getWordIds).then(function(result) {
            expect(result).toContain("1");
            expect(result).toContain("2");
            expect(result).toContain("confuser");

            done();
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
            done();
        });
    });

    async.it("get variants of a word by wildcard", function(done) {
        English.addWords(englishWords).then(getWordIds).then(getWordIds).then(function(result) {
            return English.loadWords([ "1" ]).then(function(words) {
                var variants = Variants.variantsOf(words["1"], "read", null);
                expect(variants).toContain([ "uk", "centre" ]);
                expect(variants).toContain([ "us", "center" ]);
                done();
            });

        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("get specific variants of a word ", function(done) {
        English.addWords(englishWords).then(getWordIds).then(function(result) {
            return English.loadWords([ "1" ]).then(function(words) {
                var variants = Variants.variantsOf(words["1"], "read", "uk");
                expect(variants[0]).toEqual([ "uk", "centre" ]);
                done();
            });

        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("get specific variants of a word that has wildcards ", function(done) {
        English.addWords(englishWords).then(getWordIds).done(function(result) {
            return English.loadWords([ "confuser" ]).then(function(words) {
                var variants = Variants.variantsOf(words["confuser"], "read", "uk");
                expect(variants[0]).toEqual([ Variants.getDefault(), "confuser" ]);
                done();
            }, function(error) {
                expect(true).toBe(false);
                console.log(error.stack);
                done();
            });

        });
    });

    async.it("find words by id", function(done) {
        var selector = {
            id : [ "2", "3" ]
        };

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findWords(selector);
        }).done(function(result) {
            expect(result).toContain("2");
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("find words by concept", function(done) {
        var selector = {
            concept : [ "CENTER" ]
        };

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findWords(selector);
        }).done(function(result) {
            expect(result).toContain("1");
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("find words by type", function(done) {
        var selector = {
            type : [ "phrase", "word" ]
        };

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findWords(selector);
        }).done(function(result) {
            expect(result).toContain("1");
            expect(result).toContain("2");
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("find words by tags", function(done) {
        var selector = {
            tags : [ "noun", "verb" ]
        };

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findWords(selector);
        }).done(function(result) {
            expect(result).toContain("1");
            expect(result).toContain("2");
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("find words by pronunciations", function(done) {
        var selector = {
            pronounce : [ [ "uk", "center" ] ]
        };

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findWords(selector);
        }).done(function(result) {
            expect(result).toContain("1");
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("find words by reading", function(done) {
        var selector = {
            read : [ [ null, "centre" ] ]
        };

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findWords(selector);
        }).done(function(result) {
            expect(result).toContain("1");
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("not find no matches", function(done) {
        var selector = {
            concept : [ "Good Morning" ],
            read : [ [ null, "centre" ] ]
        };

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findWords(selector);
        }).done(function(result) {
            expect(result.length).toBe(0);
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("should load words from the database", function(done) {
        var words = [ "3", "1", "2" ];

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.loadWords(words);
        }).done(function(result) {
            expect(result["1"].type).toBe("word");
            expect(result["2"].type).toBe("phrase");
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("should find confuser words without variants", function(done) {

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findConfusers("1", "read");
        }).done(function(result) {
            expect(result.length).toBe(1);
            expect(result).toContain("confuser");
            done();
        }, function(error) {
            expect(true).toBe(false);
            console.log(error.stack);
            done();
        });
    });

    async.it("should find confuser words with variants", function(done) {

        English.addWords(englishWords).then(getWordIds).then(function() {
            return English.findConfusers("1", "read", [ "uk" ]);
        }).done(function(result) {
            expect(result.length).toBe(1);
            expect(result).toContain("confuser");
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

});
