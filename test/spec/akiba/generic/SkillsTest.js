describe("Skills", function() {
    // set up the async spec
    var async = new AsyncSpec(this);

    var printResult = function(result) {
        console.log(JSON.stringify(result, null, 2));
        return result;
    };

    var English = null;
    var englishWords = null;
    var Variants = null;

    async.beforeEach(function(done) {
        // also creates the akiba.ut package
        Variants = zone("akiba.generic").get("Variants");

        englishWords = [ {
            id : "1",
            type : "word",
            concept : "CENTER",
            tags : [ "noun", "adjective" ],
            pronounce : [ [ Variants.getDefault(), "center" ] ],
            read : [ [ "us", "center" ], [ "uk", "centre" ] ]
        }, {
            id : "2",
            type : "phrase",
            concept : "Good Morning",
            pronounce : [ [ Variants.getDefault(), "Good Morning" ] ],
            read : [ [ Variants.getDefault(), "Good Morning" ] ]
        }, {
            id : "confuser",
            type : "word",
            tags : [ "verb", "adjective" ],
            concept : "confuser",
            pronounce : [ [ Variants.getDefault(), "confuser" ] ],
            read : [ [ Variants.getDefault(), "confuser" ] ]
        } ];

        zone("akiba.generic.db").get("Skills").open("en").then(function(s) {
            English = s;
            return s.clear().then(done);
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
            done();
        });
    });

    async.afterEach(function(done) {
        if (English) {
            English.close().then(done);
        }
    });

    async.it("create a skills provider", zone.inject("akiba.generic.db", [ "#done", "Skills" ], function(done, Skills) {
        Skills.open("xja").then(function(skills) {
            expect(skills.language()).toBe("xja");
            return skills.close();
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
        })["finally"](done);
    }));

    async.it("create skills", function(done) {
        English.updateSkills(englishWords).then(printResult).then(function(result) {
            expect(result).toContain("1");
            expect(result).toContain("2");
            expect(result).toContain("confuser");

            done();
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
        });
    });

    async.it("should create skills for individual words", function(done) {

        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ],
            speak : null
        }).done(function(result) {
            expect(result).toContain("1");
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.it("should loads skills for individual words", function(done) {

        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ],
        }).then(function() {
            return English.loadSkills([ "1", "2" ]);
        }).done(function(result) {
            expect(result["1"].read.uk.score).toBeDefined();
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.it("should not find a skill, because not all skills selectors match", function(done) {

        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ]
        }).then(function() {
            return English.findSkill([ {
                // ALL skill selectors MUST match!
                skill : [ [ "read", "us" ], [ "read", "uk" ] ]
            } ]);
        }).done(function(result) {
            expect(result).toBe(null);
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.it("should find a skill by skill and variant", function(done) {

        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ]
        }).then(function() {
            return English.findSkill([ {
                // no match for this selector
                skill : [ [ "read", "us" ] ]
            }, {
                // match for this selecotr
                skill : [ [ "read", "uk" ] ]
            } ]);
        }).done(function(result) {
            expect(result).not.toBe(null);
            expect(result.word).toBe("1");
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.it("should find a skill by skill that does not have a variant", function(done) {
        English.updateSkills([ englishWords[0] ], {
            read : true,
            speak : true,
        }).then(function() {
            return English.findSkill([ {
                // no match for this selector
                skill : [ [ "read", "us" ], "speak" ]
            }, {
                // match for this selecotr
                skill : [ [ "read", "uk" ], "speak" ]
            } ]);
        }).done(function(result) {
            expect(result).not.toBe(null);
            expect(result.word).toBe("1");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("should find a skill by skill and any variant", function(done) {

        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ]
        }).then(function() {
            return English.findSkill([ {
                // no match for this selector
                skill : [ [ "read", null ] ]
            } ]);
        }).done(function(result) {
            expect(result).not.toBe(null);
            expect(result.word).toBe("1");
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.it("should find a skill by type", function(done) {

        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ]
        }).then(function() {
            return English.findSkill([ {
                // match for this selecotr
                type : [ "word" ],
                skill : [ [ "read", null ] ]
            } ]);
        }).done(function(result) {
            expect(result).not.toBe(null);
            expect(result.word).toBe("1");
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.it("should find a skill by tag", function(done) {
        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ]
        }).then(function() {
            return English.findSkill([ {
                // match for this selecotr
                tags : [ "verb", "noun" ],
                skill : [ [ "read", null ] ]
            } ]);
        }).done(function(result) {
            expect(result).not.toBe(null);
            expect(result.word).toBe("1");
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.it("should find a skill by word", function(done) {

        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ]
        }).then(function() {
            return English.findSkill([ {
                // match for this selector
                word : [ "1" ],
                skill : [ [ "read", null ] ]
            } ]);
        }).done(function(result) {
            expect(result).not.toBe(null);
            expect(result.word).toBe("1");
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.it("should find a skill by concept", function(done) {

        English.updateSkills([ englishWords[0] ], {
            read : [ "uk" ]
        }).then(function() {
            return English.findSkill([ {
                // match for this selecotr
                concept : [ "CENTER" ],
                skill : [ [ "read", null ] ]
            } ]);
        }).done(function(result) {
            expect(result).not.toBe(null);
            expect(result.word).toBe("1");
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

});
