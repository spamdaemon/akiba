/**
 * 
 */
describe("akiba.lang.ja.audio", function() {

    var nullFN = function() {
        return null;
    };
    var word = {

    };
    var lang=null;

    beforeEach(function() {
        lang = {
            kanji : function(w) {
                return "KANJI";
            },
            kana : function(w) {
                return "HIRAGANA";
            }
        };
    });

    it("test creation of audio URL", zone.inject("akiba.lang.ja", function(audio) {

        var url = audio(lang, word);
        console.log("URL = " + url);
        expect(url).not.toBeNull();

    }));

    it("test creation of audio URL without kanji", zone.inject("akiba.lang.ja", function(audio) {

        lang.kanji = nullFN;
        var url = audio(lang, word);
        console.log("URL = " + url);
        expect(url).not.toBeNull();

    }));

    it("test creation of audio URL without kana", zone.inject("akiba.lang.ja", function(audio) {

        lang.kana = nullFN;
        var url = audio(lang, word);
        console.log("URL = " + url);
        expect(url).not.toBeNull();

    }));

    it("test failed creation of audio URL because no kanji or kana", zone.inject("akiba.lang.ja", function(audio) {

        lang.kanji = lang.kana = nullFN;
        var url = audio(lang, word);
        console.log("URL = " + url);
        expect(url).toBeNull();
    }));

});
