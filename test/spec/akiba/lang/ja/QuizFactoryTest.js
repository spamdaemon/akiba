/**
 * 
 */
describe("akiba.lang.ja.QuizFactory", function() {

    // set up the async spec
    var async = new AsyncSpec(this);

    async.it("test quiz factory injection", zone.inject("akiba.lang.ja", [ "QuizFactory", "#done" ], function(QuizFactory, done) {

        
        QuizFactory.create().then(function(qf) {
            expect(qf.language().language()).toBe("ja");
            done();

        }, function(error) {
            console.log(error);
            expect(true).toBe(false);
            done();
        });
    }));

});
