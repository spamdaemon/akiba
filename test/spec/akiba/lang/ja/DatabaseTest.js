/**
 * 
 */
describe("akiba.lang.ja.Database", function() {

    // set up the async spec
    var async = new AsyncSpec(this);

    var skills = null;
    var db = null;
    var native = null;

    var data = zone("test.akiba.lang.ja").get("data");

    async.beforeEach(function(done) {

        skills = zone("akiba.lang.ja").get("Skills").open();

        var ready = zone("akiba.lang.ja").get("Database").addWords(data).then(function(words) {
            return zone("akiba.lang.ja").get("Database").addSkills(words);
        });
        ready.then(function() {
            return zone("akiba.lang.ja").get("Database").open();
        }).then(function(x) {
            db = x;
            return db;
        }).then(function() {
            return zone("akiba.lang.en").get("Database").open().then(function(x) {
                return native = x;
            });
        }).done(function() {
            done();
        }, function(error) {
            console.log(error.stack);
            done();
        });
    });

    async.afterEach(function(done) {
        zone("akiba.lang.ja").get("Database").close().then(function() {
            return zone("akiba.lang.en").get("Database").close();
        }).then(done);
    });

    async.it("generate a quiz", zone.inject("akiba.lang.ja", [ "akiba.quiz.standardQuiz.Factory", "Json", "#done" ], function(QF, json, done) {

        var Q = QF.create(db, skills).then(function(qf) {
            return qf.createQuestion(native, {
                type : "standardQuiz",
                word : {
                    type : "word"
                },
                question : {
                    language : "foreign",
                    mode : "read",
                    variants : [ "kanji" ]
                },
                answer : {
                    type : "multiplechoice",
                    choices : 6,
                    language : "foreign",
                    mode : "read",
                    variants : [ "hiragana" ],
                }
            });
        });

        Q.then(function(quiz) {
            console.log(json.toString(quiz));
            expect(quiz.answer.presentation.length).toBe(6);
            done();
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
            done();
        });
    }));

    async.it("test injection of the database", zone.inject("akiba.lang.ja", [ "Utils", "Json", "#done" ], function(Utils, json, done) {
        db.findWords({
            read : [ [ "kanji", "猫" ] ]
        }).then(db.loadWords).done(function(w) {
            console.log(jasmine.getEnv().currentSpec.description + ": " + json.toString(w));
            expect(Utils.keys(w).length).not.toBe(0);
            done();
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
            done();
        });
    }));

    async.it("test search for text", zone.inject("akiba.lang.ja", [ "Utils", "Json", "#done" ], function(Utils, json, done) {
        db.searchWords("くる", 2).then(db.loadWords).done(function(w) {
            console.log(jasmine.getEnv().currentSpec.description + ": " + json.toString(w));
            w = Utils.values(w);
            expect(w.length).toBe(1);
            expect(w[0].id).toBe("車");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(true).toBe(false);
            done();
        });
    }));

});
