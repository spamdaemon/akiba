/**
 * 
 */
describe("languages", function() {

    var languageJA = {
        language : function() {
            return "ja";
        }
    };
    var languageEN = {
        language : function() {
            return "en";
        }
    };
    var languageDE = {
        language : function() {
            return "de";
        }
    };

    beforeEach(function() {

        zone("akiba.ut").get("Languages").clearLanguages();
    });

    it("register a language language", zone.inject("akiba.ut", function(Languages) {

        Languages.addLanguage(languageJA);
        Languages.addLanguage(languageEN);

        expect(Languages.languageFor("ja")).toBe(languageJA);
        expect(Languages.languageFor("en")).toBe(languageEN);
    }));

    it("retrieve a language language", zone.inject("akiba.ut", function(Languages) {
        Languages.addLanguage(languageJA);
        expect(Languages.languageFor("ja")).toBe(languageJA);
    }));

    it("return null for a language that was not registered", zone.inject("akiba.ut", function(Languages) {
        Languages.addLanguage(languageJA);
        expect(Languages.languageFor("en")).toBe(null);

    }));

    it("list all supported languages", zone.inject("akiba.ut", function(Languages) {
        Languages.addLanguage(languageJA);
        Languages.addLanguage(languageEN);
        Languages.addLanguage(languageDE);

        var list = Languages.listLanguages();
        expect(list).toContain("ja");
        expect(list).toContain("de");
        expect(list).toContain("en");
    }));

});
