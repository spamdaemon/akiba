describe("quizFactory", function() {
    // set up the async spec
    var async = new AsyncSpec(this);

    var Concepts = null;
    var Skills = null;
    var Foreign = null;
    var Native = null;
    var QuizFactory = null;
    var Utils = null;
    var nativeTag = "native";
    var foreignTag = "foreign";

    var printResult = zone("akiba").inject([ "Json", "#printEnabled" ], function(Json, enabled) {
        return function(result) {
            if (enabled) {
                console.log(Json.toString(result));
            }
            return result;
        };
    });

    async.afterEach(function(done) {
        zone("akiba.concepts").get("Concepts").close().yield(Foreign).then(function(e) {
            return e.close();
        }).yield(Native).then(function(e) {
            return e.close();
        }).yield(Skills).then(function(e) {
            return e.close();
        }).then(done);
    });

    async.beforeEach(function(done) {
        console.log("BeforeEach " + jasmine.getEnv().currentSpec.description);

        Skills = zone("akiba.generic").get("Skills").open(foreignTag);
        Concepts = zone("akiba.concepts").get("Concepts").open();
        var Language = zone("akiba.generic").get("Language");

        var Quizzes = zone("akiba.quiz.standardQuiz").get("Factory");
        var Variants = zone("akiba.generic").get("Variants");
        Utils = zone("akiba.ut").get("Utils");

        Foreign = Language.open(foreignTag, function() {
            return "http://audio/" + foreignTag;
        });
        Native = Language.open(nativeTag, function() {
            return "http://audio/" + nativeTag;
        });
        QuizFactory = Quizzes.create(Foreign, Skills);

        var englishWords = [ {
            id : "center",
            type : "word",
            concept : "center",
            tags : [ "noun" ],
            pronounce : [ [ Variants.getDefault(), "center" ] ],
            read : [ [ "us", "center" ], [ "uk", "centre" ] ]
        }, {
            id : "big",
            type : "word",
            concept : "big",
            pronounce : [ [ Variants.getDefault(), "big" ] ],
            read : [ [ Variants.getDefault(), "big" ] ]
        }, {
            id : "large",
            type : "word",
            concept : "big",
            pronounce : [ [ Variants.getDefault(), "large" ] ],
            read : [ [ Variants.getDefault(), "large" ] ]
        }, {
            id : "small",
            type : "word",
            concept : "small",
            pronounce : [ [ Variants.getDefault(), "small" ] ],
            read : [ [ Variants.getDefault(), "small" ] ]
        }, {
            id : "black",
            type : "word",
            concept : "black",
            pronounce : [ [ Variants.getDefault(), "black" ] ],
            read : [ [ Variants.getDefault(), "black" ] ]
        }, {
            id : "white",
            type : "word",
            concept : "white",
            pronounce : [ [ Variants.getDefault(), "white" ] ],
            read : [ [ Variants.getDefault(), "white" ] ]
        } ];

        var germanWords = [ {
            id : "groß",
            type : "word",
            concept : "big",
            pronounce : [ [ Variants.getDefault(), "groß" ] ],
            read : [ [ Variants.getDefault(), "groß" ] ]
        }, {
            id : "klein",
            type : "word",
            concept : "small",
            pronounce : [ [ Variants.getDefault(), "klein" ] ],
            read : [ [ Variants.getDefault(), "klein" ] ]
        }, {
            id : "Zentrum",
            type : "word",
            concept : "center",
            pronounce : [ [ Variants.getDefault(), "zentrum" ] ],
            read : [ [ Variants.getDefault(), "Zentrum" ] ]
        }, {
            id : "Mittelpunkt",
            type : "word",
            concept : "center",
            pronounce : [ [ Variants.getDefault(), "mittelpunkt" ] ],
            read : [ [ Variants.getDefault(), "Mittelpunkt" ] ]
        }, {
            id : "schwarz",
            type : "word",
            concept : "black",
            pronounce : [ [ Variants.getDefault(), "schwarz" ] ],
            read : [ [ Variants.getDefault(), "schwarz" ] ]
        }, {
            id : "weiß",
            type : "word",
            concept : "white",
            pronounce : [ [ Variants.getDefault(), "weiß" ] ],
            read : [ [ Variants.getDefault(), "weiß" ] ]
        } ];

        var concepts = [];
        Utils.each(germanWords, function(w) {
            concepts.push({
                id : w.concept,
                images : [ "IMAGES/" + w.concept ]
            });
        });
        Utils.each(englishWords, function(w) {
            concepts.push({
                id : w.concept,
                images : [ "IMAGES/" + w.concept ]
            });
        });
        Concepts.then(function(c) {
            return c.addConcepts(concepts);
        }).yield(Native).then(function(lang) {
            return lang.addWords(germanWords);
        }).yield(Foreign).tap(function(lang) {
            return lang.addWords(englishWords);
        }).then(function(lang) {
            return lang.loadWords([ "center", "big", "large", "small" ]).then(function(ww) {
                return Skills.then(function(s) {
                    return s.updateSkills(ww, {
                        read : true,
                        listen : true,
                        write : true,
                        speak : true,
                        recognize : true,
                        translate : true
                    }).then(function(result) {
                        return s.loadSkills(result);
                    });
                });
            });
        }).then(printResult()).done(function() {
            done();
        });
    });

    async.it("create a quiz factory", zone.inject("akiba.quiz.standardQuiz", [ "Factory", "#done" ], function(QF, done) {
        QF.create(Foreign, Skills).then(function(f) {
            expect(f.language().language()).toBe(foreignTag);
            return null;
        }, function(error) {
            expect(false).toBe(true);
        })["finally"](done);
    }));

    async.it("smoketest:create a quiz", function(done) {
        var N = 3;
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ],
                },
                question : {
                    language : nativeTag,
                    mode : "read"
                },
                answer : {
                    language : foreignTag,
                    mode : "read",
                    choices : N,
                    type : "multiplechoice",
                    variants : [ "uk", "us" ]
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.answer.presentation.length).toBe(N);
            expect(Utils.countBy(result.answer.presentation, function(x) {
                return x.correct ? "TRUE" : "FALSE";
            }).TRUE).toBe(1);

            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(NATIVE) - read(FOREIGN)", function(done) {
        var N = 4;
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "read"
                },
                answer : {
                    language : foreignTag,
                    mode : "read",
                    choices : N,
                    type : "multiplechoice",
                    variants : [ "uk", "us" ]
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.question.presentation.text).not.toBeNull();
            expect(result.answer.presentation[0].text).not.toBeNull();
            expect(result.answer.presentation.length).toBe(N);
            expect(Utils.countBy(result.answer.presentation, function(x) {
                return x.correct ? "TRUE" : "FALSE";
            }).TRUE).toBe(1);

            done();
        }, function(error) {
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(NATIVE) - write(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "read"
                },
                answer : {
                    language : foreignTag,
                    mode : "write",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.answer.presentation.mode).toBe("write");
            done();
        }, function(error) {
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(FOREIGN) - write(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "read",
                    variants : [ "us" ]
                },
                answer : {
                    language : foreignTag,
                    mode : "write",
                    variants : [ "uk" ]
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.question.presentation.selector.value).toEqual([ "us", "center" ]);
            expect(result.answer.presentation.mode).toBe("write");
            done();
        }, function(error) {
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(NATIVE) - speak(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "read"
                },
                answer : {
                    language : foreignTag,
                    mode : "speak",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.answer.presentation.mode).toBe("speak");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz listen(NATIVE) - read(FOREIGN)", function(done) {
        var N = 4;
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "listen"
                },
                answer : {
                    language : foreignTag,
                    mode : "read",
                    type : "multiplechoice",
                    choices : N,
                    variants : [ "uk", "us" ]
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("listen");
            expect(result.question.presentation.audio.length).not.toBe(0);
            expect(result.answer.presentation[0].text).not.toBeNull();
            expect(result.answer.presentation.length).toBe(N);
            expect(Utils.countBy(result.answer.presentation, function(x) {
                return x.correct ? "TRUE" : "FALSE";
            }).TRUE).toBe(1);
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz listen(NATIVE) - write(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "listen"
                },
                answer : {
                    language : foreignTag,
                    mode : "write",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("listen");
            expect(result.question.presentation.audio.length).not.toBe(0);
            expect(result.answer.presentation.mode).toBe("write");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz listen(NATIVE) - speak(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "listen"
                },
                answer : {
                    language : foreignTag,
                    mode : "speak",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("listen");
            expect(result.question.presentation.audio.length).not.toBe(0);
            expect(result.answer.presentation.mode).toBe("speak");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz show - read(FOREIGN)", function(done) {
        var N = 4;
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "show"
                },
                answer : {
                    language : foreignTag,
                    mode : "read",
                    type : "multiplechoice",
                    choices : N,
                    variants : [ "uk", "us" ]
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("show");
            expect(result.question.presentation.images.length).not.toBe(0);
            expect(result.question.presentation.images[0]).toBeDefined();
            expect(result.answer.presentation[0].text).not.toBeNull();
            expect(result.answer.presentation.length).toBe(N);
            expect(Utils.countBy(result.answer.presentation, function(x) {
                return x.correct ? "TRUE" : "FALSE";
            }).TRUE).toBe(1);
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz show - write(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "show"
                },
                answer : {
                    language : foreignTag,
                    mode : "write",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("show");
            expect(result.question.presentation.images.length).not.toBe(0);
            expect(result.question.presentation.images[0]).toBeDefined();
            expect(result.answer.presentation.mode).toBe("write");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz show - speak(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : nativeTag,
                    mode : "show"
                },
                answer : {
                    language : foreignTag,
                    mode : "speak",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(nativeTag);
            expect(result.answer.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("show");
            expect(result.question.presentation.images.length).not.toBe(0);
            expect(result.question.presentation.images[0]).toBeDefined();
            expect(result.answer.presentation.mode).toBe("speak");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(FOREIGN) - read(NATIVE)", function(done) {
        var N = 4;
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "read"
                },
                answer : {
                    language : nativeTag,
                    mode : "read",
                    choices : N,
                    type : "multiplechoice",
                    variants : [ "uk", "us" ]
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.answer.language).toBe(nativeTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.answer.presentation[0].text).not.toBeNull();
            expect(result.answer.presentation.length).toBe(N);
            expect(Utils.countBy(result.answer.presentation, function(x) {
                return x.correct ? "TRUE" : "FALSE";
            }).TRUE).toBe(1);

            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(FOREIGN) - show()", function(done) {
        var N = 4;
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "read"
                },
                answer : {
                    mode : "show",
                    choices : N,
                    type : "multiplechoice",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.answer.presentation.length).toBe(N);
            expect(result.answer.presentation[0].images.length).not.toBe(0);
            expect(Utils.countBy(result.answer.presentation, function(x) {
                return x.correct ? "TRUE" : "FALSE";
            }).TRUE).toBe(1);
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(FOREIGN) - write(NATIVE)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "read"
                },
                answer : {
                    language : nativeTag,
                    mode : "write",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.answer.language).toBe(nativeTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.answer.presentation.mode).toBe("write");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(FOREIGN) - speak(NATIVE)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "read"
                },
                answer : {
                    language : nativeTag,
                    mode : "speak",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.answer.language).toBe(nativeTag);
            expect(result.question.presentation.mode).toBe("read");
            expect(result.answer.presentation.mode).toBe("speak");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz listen(FOREIGN) - read(NATIVE)", function(done) {
        var N = 4;
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "listen"
                },
                answer : {
                    language : nativeTag,
                    mode : "read",
                    choices : N,
                    type : "multiplechoice",
                    variants : [ "uk", "us" ]
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.answer.language).toBe(nativeTag);
            expect(result.question.presentation.mode).toBe("listen");
            expect(result.question.presentation.audio.length).not.toBe(0);
            expect(result.answer.presentation.length).toBe(N);
            expect(result.answer.presentation[0].text).not.toBeNull();
            expect(Utils.countBy(result.answer.presentation, function(x) {
                return x.correct ? "TRUE" : "FALSE";
            }).TRUE).toBe(1);

            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz listen(FOREIGN) - show()", function(done) {
        var N = 4;
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "listen"
                },
                answer : {
                    mode : "show",
                    choices : N,
                    type : "multiplechoice",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.question.presentation.mode).toBe("listen");
            expect(result.question.presentation.audio.length).not.toBe(0);
            expect(result.answer.presentation.length).toBe(N);
            expect(result.answer.presentation[0].images.length).not.toBe(0);
            expect(Utils.countBy(result.answer.presentation, function(x) {
                return x.correct ? "TRUE" : "FALSE";
            }).TRUE).toBe(1);

            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("selftest quiz answers read(FOREIGN) - write(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "read",
                    variants : [ "us" ]
                },
                answer : {
                    language : foreignTag,
                    mode : "write",
                    variants : [ "uk" ],
                    type : "selftest"
                },
            }).then(printResult(true)).done(function(quiz) {
                expect(quiz.answer.presentation[0].text[1]).toBe("centre");
                done();
            }, function(error) {
                console.log(error.stack);
                expect(false).toBe(true);
                done();
            });
        });
    });

    async.it("create quiz listen(FOREIGN) - write(NATIVE)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "listen"
                },
                answer : {
                    language : nativeTag,
                    mode : "write",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.answer.language).toBe(nativeTag);
            expect(result.question.presentation.mode).toBe("listen");
            expect(result.question.presentation.audio.length).not.toBe(0);
            expect(result.answer.presentation.mode).toBe("write");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz listen(FOREIGN) - speak(NATIVE)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "listen"
                },
                answer : {
                    language : nativeTag,
                    mode : "speak",
                },
            });
        }).then(printResult()).done(function(result) {
            expect(result.question.language).toBe(foreignTag);
            expect(result.answer.language).toBe(nativeTag);
            expect(result.question.presentation.mode).toBe("listen");
            expect(result.question.presentation.audio.length).not.toBe(0);
            expect(result.answer.presentation.mode).toBe("speak");
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("test corrent answers listen(FOREIGN) - speak(NATIVE)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "listen"
                },
                answer : {
                    language : nativeTag,
                    mode : "speak",
                },
            });
        }).then(printResult()).then(function(result) {
            result.answer.presentation.capture.variant = null;
            result.answer.presentation.capture.value = "Mittelpunkt";
            return QuizFactory.then(function(qf) {
                return qf.answerQuestion(Native, result, [ result.answer.presentation.capture ]);
            });
        }).done(function(result) {
            console.log(JSON.stringify(result, 0, 2));
            expect(result[0].correct).toBe(true);
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("test wrong answers listen(FOREIGN) - speak(NATIVE)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "listen"
                },
                answer : {
                    language : nativeTag,
                    mode : "speak",
                },
            });
        }).then(printResult()).then(function(result) {
            result.answer.presentation.capture.submit = true;
            result.answer.presentation.capture.variant = null;
            result.answer.presentation.capture.value = "Center";
            return QuizFactory.then(function(qf) {
                return qf.answerQuestion(Native, result, [ result.answer.presentation.capture ]);
            });
        }).done(function(result) {
            expect(result[0].correct).toBe(false);
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("create quiz read(FOREIGN) - write(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "read",
                    variants : [ "us" ]
                },
                answer : {
                    language : foreignTag,
                    mode : "write",
                    variants : [ "uk" ]
                },
            });
        }).then(printResult()).then(function(result) {
            result.answer.presentation.capture.variant = "uk";
            result.answer.presentation.capture.value = "centre";
            return QuizFactory.then(function(qf) {
                return qf.answerQuestion(Native, result, [ result.answer.presentation.capture ]);
            });
        }).done(function(result) {
            expect(result[0].correct).toBe(true);
            done();
        }, function(error) {
            console.log(error.stack);
            expect(false).toBe(true);
            done();
        });
    });

    async.it("find quiz answers read(FOREIGN) - write(FOREIGN)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "read",
                    variants : [ "us" ]
                },
                answer : {
                    language : foreignTag,
                    mode : "write",
                    variants : [ "uk" ]
                },
            }).then(function(quiz) {
                return qf.findPossibleAnswers(Native, quiz);
            }).then(printResult(true)).done(function(result) {
                expect(result[0].text[1]).toBe("centre");
                done();
            }, function(error) {
                console.log(error.stack);
                expect(false).toBe(true);
                done();
            });
        });
    });

    async.it("test wrong answers listen(FOREIGN) - speak(NATIVE)", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "listen"
                },
                answer : {
                    language : nativeTag,
                    mode : "speak",
                },
            }).then(function(quiz) {
                return qf.findPossibleAnswers(Native, quiz);
            }).then(printResult(true)).done(function(result) {
                expect(result.length).toBe(2);
                expect((result[0].text[1] == "Mittelpunkt") || (result[0].text[1] == "Zentrum")).toBe(true);
                done();
            }, function(error) {
                console.log(error.stack);
                expect(false).toBe(true);
                done();
            });
        });
    });

    async.it("test wrong answers listen(FOREIGN) - show()", function(done) {
        QuizFactory.then(function(qf) {
            return qf.createQuestion(Native, {
                type : "standardQuiz",
                word : {
                    type : "word",
                    tags : [ "noun" ]
                },
                question : {
                    language : foreignTag,
                    mode : "listen"
                },
                answer : {
                    mode : "show",
                    choices : 1,
                    type : "multiplechoice",
                },
            }).then(function(quiz) {
                return qf.findPossibleAnswers(Native, quiz);
            }).then(printResult(true)).done(function(result) {
                expect(result.length).toBe(2);
                expect((result[0].text[1] == "Mittelpunkt") || (result[0].text[1] == "Zentrum")).toBe(true);
                done();
            }, function(error) {
                console.log(error.stack);
                expect(false).toBe(true);
                done();
            });
        });
    });

});
